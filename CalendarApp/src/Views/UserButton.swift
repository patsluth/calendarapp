//
//  UserButton.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2018-10-07.
//  Copyright © 2018 Pat Sluth. All rights reserved.
//

import UIKit

//import Firebase
//import FirebaseAuth
//import FirebaseFirestore
import RxSwift
import RxCocoa
import RxSwiftExt





final class UserButton: UIButton
{
//	func set(user: User?, disposeBag: DisposeBag)
//	{
//		guard let user = user else {
//			self.setTitle("Login", for: .normal)
//			return
//		}
//
//		let field = user.document.fieldOf(Profile.self)
//
//		_ = field.valueObservable()
//			//.take(1)
//			.do(onNext: { [weak self] result in
//				switch result {
//				case .Success(let value):
//					let firstLetter = value.firstName.first ?? Character(" ")
//					let lastLetter = value.lastName.first ?? Character(" ")
//					self?.setTitle("\(firstLetter)\(lastLetter)", for: .normal)
//				case .Failure( _ as NSError):
//					self?.setTitle("!", for: .normal)
//				}
//			})
//			.subscribe()
//			.disposed(by: disposeBag)
//	}
	
	override func willMove(toSuperview newSuperview: UIView?)
	{
		super.willMove(toSuperview: newSuperview)
		
		self.layer.masksToBounds = true
		self.layer.borderWidth = 2.0
		self.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 4.0, bottom: 0.0, right: 4.0)
		
		self.titleLabel?.minimumScaleFactor = 0.1
		self.titleLabel?.numberOfLines = 1
		self.titleLabel?.adjustsFontSizeToFitWidth = true
		self.titleLabel?.textAlignment = NSTextAlignment.center
		self.titleLabel?.lineBreakMode = NSLineBreakMode.byClipping
		self.titleLabel?.baselineAdjustment = UIBaselineAdjustment.alignCenters
	}
	
	override func tintColorDidChange()
	{
		super.tintColorDidChange()
		
		self.layer.borderColor = self.tintColor.cgColor
	}
	
	override func layoutSubviews()
	{
		super.layoutSubviews()
		
		self.layer.cornerRadius = self.bounds.midX
	}
}




