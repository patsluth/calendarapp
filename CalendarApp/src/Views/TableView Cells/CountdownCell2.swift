//
//  CountdownCell2.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2017-08-07.
//  Copyright © 2017 patsluth. All rights reserved.
//

import UIKit
import EventKit

import Sluthware
import RxSwift
import RxCocoa
import SwiftDate





final class CountdownCell2: UITableViewCellBase, ModelView
{
	typealias Model = EKEvent
	
	
	
	static var nib: UINib? {
		return UINib(resource: R.nib.countdownCell2)
	}
	
	
	
	@IBOutlet private var titleLabel: UILabel!
	@IBOutlet private var timeValueLabel: UILabel!
	@IBOutlet private var timeComponentLabel: UILabel!
	
	private var timeComponents: [TimeComponent]!
	
	
	
	
	
	override func awakeFromNib()
	{
		super.awakeFromNib()
		
		self.timeComponents = [.year,
							   .month,
							   .week,
							   .day,
							   .hour,
							   .minute,
							   .second]
	}
	
	override func prepareForReuse()
	{
		super.prepareForReuse()
	}
	
	func update(with model: EKEvent)
	{
//		switch model {
//		case .Success(let value):
		
		let now = Date()
		let timeInterval = model.startDate.date.timeIntervalSince(now)
		
		let calendar = Calendar.current
		let calendarComponents = self.timeComponents.map({ $0.calendarComponent })
		let dateComponents = calendar.dateComponents(Set(calendarComponents),
													 from: now,
													 to: model.startDate)
		
		let timeComponent = self.timeComponents.first(where: {
			let value = dateComponents.value(for: $0.calendarComponent) ?? 0
			return (value != 0)
		}) ?? TimeComponent.nanosecond
		
		
		
		let fraction = Fraction(timeInterval / timeComponent.timeInterval)
			.roundedTo(den: 4)
		let mixedNumber = fraction.asMixedNumber(reduced: true)
		let formatter = MixedNumberFormatter() {
			$0.font = self.timeValueLabel.font
		}
		
		self.titleLabel.text = model.title
		self.timeValueLabel.attributedText = formatter.format(mixedNumber)
		self.timeComponentLabel.text = {
			let string = "\(timeComponent)"
			return string + ((fraction.isProper) ? "" : "s")
		}()
			
//		case .Failure(let error):
//
//			self.titleLabel.text = "Error"
//			self.timeValueLabel.text = "!"
//			self.timeComponentLabel.text = "\(type(of: error))"
//
//		}
		
		
//		switch model {
//		case .Success(let value):
//
//			let now = Date()
//			let timeInterval = value.event.startDate.date.timeIntervalSince(now)
//
//			let calendar = Calendar.current
//			let dateComponents = calendar.dateComponents(Set(self.calendarComponents),
//														 from: now,
//														 to: value.event.startDate)
//
//			let calendarComponent = self.calendarComponents.first(where: {
//				$0.timeInterval
//				let value = dateComponents.value(for: $0) ?? 0
//				return (value != 0)
//			}) ?? self.calendarComponents.last
//
//
//			let fraction = Fraction(timeInterval / (calendarComponent!.timeInterval ?? 0.0))
//				.roundedTo(den: 4)
//			let mixedNumber = fraction.asMixedNumber(reduced: true)
//			let formatter = MixedNumberFormatter() {
//				$0.font = self.countdownLabel.font
//			}
//			//		let decimalPlaces = 1
//			//		let valueString = String(format: "%.\(decimalPlaces)f", value.rounded(to: decimalPlaces))
//
//			self.titleLabel.text = value.event.title
//			self.countdownLabel.attributedText = formatter.format(mixedNumber)
//			self.dateComponentLabel.text = {
//				var string = "\(calendarComponent!)"
//				string = string.camelCaseToWords.first ?? string
//				return string + ((fraction.magnitude <= 1.0) ? "" : "s")
//			}()
//
//		case .Failure(let error):
//
//			self.titleLabel.text = "\(type(of: error))"
//			self.countdownLabel.text = "!"
//			self.dateComponentLabel.text = "Error"
//
//		}
	}
}




