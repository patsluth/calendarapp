//
//  EKCalendarCell.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2017-08-07.
//  Copyright © 2017 patsluth. All rights reserved.
//

import UIKit
import EventKit





final class EKCalendarCell: UITableViewCellBase, ModelView
{
	typealias Model = EKCalendar
	
	
	
	static var nib: UINib? {
		return UINib(resource: R.nib.ekCalendarCell)
	}
	
	
	
	
	@IBOutlet private var colorView: UIView!
	@IBOutlet private var titleLabel: UILabel!
	
	
	
	
	
	override func awakeFromNib()
	{
		super.awakeFromNib()
	}
	
	override func prepareForReuse()
	{
		super.prepareForReuse()
	}
	
	override func layoutSubviews()
	{
		super.layoutSubviews()
		
		self.colorView.layer.masksToBounds = true
		self.colorView.layer.cornerRadius = self.colorView.bounds.midX
	}
	
	func update(with model: EKCalendar)
	{
		self.colorView.backgroundColor = UIColor(cgColor: model.cgColor)
		self.titleLabel.text = model.title
	}
}




