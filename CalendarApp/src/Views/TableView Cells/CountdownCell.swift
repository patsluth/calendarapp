//
//  CountdownCell.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2017-08-07.
//  Copyright © 2017 patsluth. All rights reserved.
//

import UIKit
import EventKit

import RxSwift
import RxCocoa


public protocol BlockInitializable
{
	init()
}

//public protocol BlockInitializable2
//{
//	associatedtype InitValue
//
//	init(_ initValue: InitValue, _ block: (Self) -> Void)
//}

//extension BlockInitializable2
//{
//	init(_ initValue: InitValue, _ block: (Self) -> Void)
//	{
//		block(self)
//	}
//}



public extension BlockInitializable
{
	init(_ block: (Self) -> Void)
	{
		self.init()
		
		defer {
			block(self)
		}
	}
}

extension UILabel: BlockInitializable
{
	
}





final class CountdownCell: RxTableViewCell
{
	@IBOutlet private var titleLabel: UILabel!
	@IBOutlet private var valueStackView: UIStackView!
	
	var calendarComponents: [Calendar.Component]! {
		didSet
		{
			//			guard self.calendarComponents != oldValue else { return }
			
			self.valueStackView.subviews.forEach {
				$0.removeFromSuperview()
			}
			
			for calendarComponent in self.calendarComponents {
				let label = UILabel() {
					$0.translatesAutoresizingMaskIntoConstraints = false
					$0.textAlignment = NSTextAlignment.center
					$0.numberOfLines = 2
					//					$0.adjustsFontSizeToFitWidth = true
					//					$0.minimumScaleFactor = 0.5
					$0.font = UIFont.systemFont(ofSize: 13, weight: .light)
				}
				
				label.tag = Int(calendarComponent.timeInterval!)
				self.valueStackView.addArrangedSubview(label)
			}
		}
	}
	
	
	
	
	
	override func awakeFromNib()
	{
		super.awakeFromNib()
		
		self.calendarComponents = [//.year,
			//.month,
			//.weekOfMonth,
			.day,
			.hour,
			.minute,
			.second]
	}
	
	override func prepareForReuse()
	{
		super.prepareForReuse()
	}
	
	//	enum TimeUnit: Double
	//	{
	//		case Second = 1.0
	//		case Minute = 60.0
	//		case Hour = 3600.0
	//		case Day = 86400.0
	//		case Week = 604800.0
	//
	//		func convertTo(_ timeUnit: TimeUnit) -> Double
	//		{
	//			return self.rawValue / timeUnit.rawValue
	//		}
	//	}
	//
	//	print(TimeUnit.Day.convertTo(TimeUnit.Hour) == 24.0)
	
	func set(event: EKEvent)
	{
		let dateComponents = Calendar.current.dateComponents(Set(self.calendarComponents),
															 from: Date(),
															 to: event.startDate)
		
		
		
		self.titleLabel.text = event.title
		
		for calendarComponent in self.calendarComponents {
			let label = self.valueStackView.viewWithTag(Int(calendarComponent.timeInterval!)) as! UILabel
			let value = dateComponents.value(for: calendarComponent) ?? 0
			let suffix: String = {
				var string = "\(calendarComponent)"
				string = string.camelCaseToWords.first ?? string
				return "\(string)s"
			}()
			
			label.text = "\(value)\(Char.NewLine)\(suffix)"
		}
	}
}

