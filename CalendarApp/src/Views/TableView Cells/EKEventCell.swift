//
//  EKEventCell.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2017-08-07.
//  Copyright © 2017 patsluth. All rights reserved.
//

import UIKit
import EventKit

import Sluthware
import RxSwift
import RxCocoa





final class EKEventCell: UITableViewCellBase, ModelView
{
	typealias Model = EKEvent
	
	
	
	static var nib: UINib? {
		return UINib(resource: R.nib.ekEventCell)
	}
	
	
	
	@IBOutlet private var titleLabel: UILabel!
	@IBOutlet private var detailLabel: UILabel!
	
	
	
	
	
	override func awakeFromNib()
	{
		super.awakeFromNib()
	}
	
	override func prepareForReuse()
	{
		super.prepareForReuse()
	}
	
	func update(with model: EKEvent)
	{
		self.titleLabel.text = model.title
		let f = DateFormatter()
		f.dateStyle = .long
		self.detailLabel.text = Char.Tab + f.string(from: model.startDate)
	}
}




