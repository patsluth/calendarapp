//
//  AppDelegate.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2018-10-27.
//  Copyright © 2018 Pat Sluth. All rights reserved.
//

import UIKit

//import Firebase
//import FirebaseFirestore





@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
	var window: UIWindow?
	
	static var shared: AppDelegate {
		return UIApplication.shared.delegate as! AppDelegate
	}
	
	
	
	
	
	func application(_ application: UIApplication,
					 didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
	{
		Shared.initialize()
		
		application.isIdleTimerDisabled = true
		
		return true
	}
	
	func applicationWillEnterForeground(_ application: UIApplication)
	{
		print(#file.fileName, #function)
	}
	
	func applicationDidBecomeActive(_ application: UIApplication)
	{
		print(#file.fileName, #function)
	}
	
	func applicationDidEnterBackground(_ application: UIApplication)
	{
		print(#file.fileName, #function)
	}
	
	func applicationWillResignActive(_ application: UIApplication)
	{
		print(#file.fileName, #function)
	}
	
	func applicationWillTerminate(_ application: UIApplication)
	{
		print(#file.fileName, #function)
	}
	
	func application(_ app: UIApplication,
					 open url: URL,
					 options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool
	{
		// TODO: See SWQuestrade
		
		return false
	}
}




