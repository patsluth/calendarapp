//
//  Shared.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2018-01-05.
//  Copyright © 2018 patsluth. All rights reserved.
//

import Foundation
import EventKit

@_exported import Sluthware

//import Firebase
import RxSwift
import RxCocoa
import RxSwiftExt
//import KeychainAccess





final class Shared
{
	static let applicationGroupIdentifier = "group.com.patsluth.CalendarApp"
	
	static let userDefaults = UserDefaults(suiteName: Shared.applicationGroupIdentifier)!
	
	
	
	class func initialize()
	{
//		let appKeychain = Keychain(service: "firebase_auth_1:300193149810:ios:e6548ac01b094b68")
//		let extensionKeychain = Keychain(service: "firebase_auth_1:300193149810:ios:a92038191d6996d1")
//		let userKeychainKey = "firebase_auth_1___FIRAPP_DEFAULT_firebase_user"
//
//		if FirebaseApp.app() == nil {
//			FirebaseApp.configure()
//
//			Firestore.firestore().settings = {
//				let settings = Firestore.firestore().settings
//				settings.isPersistenceEnabled = true
////				settings.areTimestampsInSnapshotsEnabled = true
//				return settings
//			}()
//		}
//
//		// Sync keychains
//		do {
//			if let data = try appKeychain.getData(userKeychainKey) {
//				try extensionKeychain.set(data, key: userKeychainKey)
//			} else {
//				try extensionKeychain.remove(userKeychainKey)
//			}
//		} catch {
//			print(#file.fileName, #function, #line, error)
//		}
	}
}



extension EKEventStore
{
	static let `default` = EKEventStore()
}




extension UserDefaults
{
//	struct Variables
//	{
	@objc dynamic static var hiddenEventIds: Set<String> {
		get
		{
			let data = Shared.userDefaults.data(forKey: #keyPath(UserDefaults.hiddenEventIds))
			let set = try? Set<String>.decode(data)
			return set ?? []
		}
		set
		{
			let data = try? newValue.encode(Data.self)
			Shared.userDefaults.set(data, forKey: #keyPath(UserDefaults.hiddenEventIds))
		}
	}
		
//		static let primaryButtonDisplayTypeObservable = Shared.userDefaults.rx
//			.observe(QuoteCell.PrimaryButtonDisplayType.self, #keyPath(QuoteCell.primaryButtonDisplayType))
//			.startWith(QuoteCell.primaryButtonDisplayType)
//			.distinctUntilChanged()
//			.unwrap()
//	}
}




