//
//  CountdownViewController.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2018-10-27.
//  Copyright © 2018 Pat Sluth. All rights reserved.
//

import UIKit
import EventKit
import NotificationCenter

//import Firebase
//import FirebaseAuth
//import FirebaseFirestore
import RxSwift
import RxCocoa
import RxSwiftExt
//import SwiftDate
import Permission
import PromiseKit





class CountdownViewController: CountdownBaseViewController
{
//	private var batch: WriteBatch!
	
	
	
	
	
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
		
		
		
//		self.batch = Firestore.firestore().batch()
		
		
		
		let userButton: UserButton = {
			let userButton = UserButton(type: UIButton.ButtonType.system)
			userButton.translatesAutoresizingMaskIntoConstraints = false
			NSLayoutConstraint.activate([
				userButton.widthAnchor.constraint(equalTo: userButton.heightAnchor),
				])
			return userButton
		}()
		
		self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: userButton)]
		
		userButton.rx.tap
			.asDriver()
			.throttle(0.5)
			.drive(onNext: { [unowned self] in
//				let userViewController = R.storyboard.main.userViewController()!
//				self.show(userViewController, sender: userButton)
				//self.navigationController?.navigationBar.prefersLargeTitles
				let navigationController = R.storyboard.main.defaultNavigationController()!
//				navigationController.navigationBar.prefersLargeTitles = true
				
				
				
//				func calendarSelectorViewController(for source: EKSource) -> DataProviderViewController<EKSource, EKCalendarCell>
//				{
//
//				}
				/*
				func firebaseViewController() -> UIViewController
				{
					let viewController = DataProviderViewController2(dataProviderType: UITableViewRowDataProvider<CountdownCell2>.self)
					
					viewController.tableView.proxyDelegate = MultipleSelectionProxy()
//					let selectionProxy = SingleSelectionProxy(viewController.tableView)
					
					
					
					//let proxy = MultipleSelectionProxy(viewController.tableView)
					let dataProvider = viewController.dataProvider
//					let dataProvider = UITableViewRowDataProvider<CountdownCell2>()

//					let `self` = DataProviderViewController2<CountdownCell2>()//{ `self` in
//
//					self.navigationItem.title = "Test"

					_ = Auth.auth().rx.currentUser()
						.do(onNext: { [unowned dataProvider] user in
							dataProvider.removeAll()
						})
						.unwrap()
						.map({ user in
							user.document.collectionOf(EventIdentifier.self)
						})
						.flatMapLatest({ collection in
							collection.observable(queryProvider: {
								$0.order(by: "\(EventIdentifier.CodingKeys.startDate.rawValue)")
							})
						})
						//			.filter({ !$0.isEmpty })
						.takeUntil(dataProvider.rx.deallocated)
						.logOnDispose()
						.bind(onNext: { [unowned dataProvider] elements in

							elements.forEach {
								let oldIP = IndexPath(row: Int($0.change.oldIndex), section: 0)
								let newIP = IndexPath(row: Int($0.change.newIndex), section: 0)

								switch $0.change.type {
								case .added:
									dataProvider.insert($0.snapshot.result, at: newIP.row)
								case .modified:
									dataProvider.remove(at: oldIP.row)
									dataProvider.insert($0.snapshot.result, at: newIP.row)
								case .removed:
									dataProvider.remove(at: oldIP.row)
								}
							}

							//								self.widgetUpdatePreferredContentSize()
						})
//						.disposed(by: dataProvider.disposeBag)
					//							.disposed(by: self.disposeBag)


//					})

//					_ = viewController.selection
//						.takeUntil(viewController.rx.deallocated)
//						.bind(onNext: { [unowned viewController] calendar in
//							viewController.show(eventSelectionViewController(calendar), sender: nil)
//						})

					return viewController
//					return self
				}
				*/
				
				typealias Pair<T, V> = (T, V)
				
				func calendarSelector(with eventStore: EKEventStore,
									  from: UIViewController,
									  _ block: @escaping (UIViewController, EKCalendar) -> Void)
				{
//					let dataSource = ModelDataSourceProxy<EKSource, EKCalendarCell>({
////						$0.headerTitleKeyPath = \EKSource.title
//						$0.headerTitleProvider = { $0.title }
//					})
					let dataSource = ModelDataSourceProxy<EKSource, EKCalendarCell>()
					dataSource.configure({
						$0.headerTitleProvider = { $0.title }
						
//						$0.selection = .Single({ model in
//							print(model)
//							print()
//						})
//
//						$0.selection = .Multiple({ models in
//							for model in models {
//								print(model.title)
//							}
//							print()
//						})
					})
					
					
//					dataSource.singleSelection = { model in
//						print(model.title)
//					}
//
//					dataSource.multipleSelection = { models in
//						for model in models {
//							print(model.title)
//						}
//					}
					
					
					let delegate = SingleSelectionProxy()
					
					let viewController = DataProviderViewController3()
					viewController.configure({
						$0.navigationItem.title = "Calendars"
						
						$0.tableView.proxyDataSource = dataSource
						$0.tableView.proxyDelegate = delegate
					})
					
					let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)
					viewController.navigationItem.rightBarButtonItem = doneButton
					_ = doneButton.rx.tap
						.take(1)
						.debug()
//						.takeUntil(doneButton.rx.deallocated)
//						.logOnDispose()
						.bind(onNext: { [unowned viewController] in
							viewController.dismiss(animated: true, completion: nil)
						})
					
					
					
					EKEventStore.default.sources
						.sorted(by: \EKSource.title.localizedLowercase, <)
						.forEach({ source in
							let calendars = source.calendars(for: .event)
								.sorted(by: \EKCalendar.title.localizedLowercase, <)
							
//							let section = ModelSection(source, {
//								calendars
//							})
							
							dataSource.append(ModelSection(source, {
								calendars
							}))
						})
					
					
//					dataSource.bind(to: delegate)
//						.unwrap()
//						.takeUntil(viewController.rx.deallocated)
//						.bind(onNext: { [unowned viewController] calendar in
//							block(viewController, calendar)
//						})
//						.takeUntil(viewController.rx.deallocated)
					
//						.unwrap().
						
					
					
					
//					return Observable.just(viewController)
//						.map({ [unowned dataSource, delegate] viewController in
//							dataSource.bind(to: delegate)
//								.unwrap()
//								.takeUntil(viewController.rx.deallocated)
//								.take(1)
//								.map({
//									(viewController, $0)
//								})
//						})
//					.takeUntil(viewController.rx.deallocated)
					
//					return dataSource.bind(to: delegate)
//						.unwrap()
//						.takeUntil(viewController.rx.deallocated)
//						.take(1)
//						.map({ [unowned viewController] selection in
//							(viewController, $0)
//						})
//						.flatMap({ [unowned dataSource, delegate] viewController in
//
//						})
//						.takeUntil(viewController.rx.deallocated)
					
					_ = dataSource.bind(to: delegate).unwrap()
						.or(doneButton.rx.tap.take(1))
						.do(onNext: { [unowned viewController] pair in
							switch pair {
							case .A(let a):
								block(viewController, a)
							case .B(_):
								viewController.dismiss(animated: true, completion: nil)
							}
						})
						.takeUntil(viewController.rx.deallocated)
						.debug()
						.subscribe()
					
					from.show(viewController, sender: nil)
				}
				
				func eventSelector(with calendar: EKCalendar,
								   from: UIViewController,
								   _ block: @escaping (UIViewController, [EKEvent]) -> Void)
				{
//					let dataSource = ModelDataSourceProxy<Int, EKEventCell>({
//						$0.headerTitleProvider = { "\($0)" }
////						$0.headerTitleKeyPath = \Date.year
//					})
					let dataSource = ModelDataSourceProxy<Int, EKEventCell>()
					dataSource.configure({
						$0.headerTitleProvider = { "\($0)" }
					})
					let delegate = MultipleSelectionProxy()
					
					let viewController = DataProviderViewController3()
					viewController.configure({
						$0.navigationItem.title = calendar.title
						
						$0.tableView.proxyDataSource = dataSource
						$0.tableView.proxyDelegate = delegate
					})
					
					let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)
					viewController.navigationItem.rightBarButtonItem = doneButton
//					_ = doneButton.rx.tap
//						.take(1)
//						.logOnDispose()
//						.bind(onNext: { [unowned viewController] in
//							viewController.dismiss(animated: true, completion: nil)
//						})
					
					
					
					let dateInterval = DateInterval(start: Date().addingTimeInterval(TimeInterval.year * -1), end: Date().addingTimeInterval(TimeInterval.year * 2))
					
					
					EKEventStore.default
						.events(from: calendar, for: dateInterval)
						.grouped(by: \EKEvent.startDate!.year)
						//							.grouped(by: \EKEvent.eventIdentifier)
						//							.compactMap({ $0.value.first })
						//							.grouped(by: \EKEvent.startDate!.year)
						.sorted(by: <, { value in
							value.sorted(by: \EKEvent.startDate, <)
						})
//						.map({ year, events in
//							(Date(year: year, month: 0, day: 0, hour: 0, minute: 0), events)
//						})
					.forEach({ year, events in
						dataSource.append(ModelSection(year, {
							events
						}))
					})
					
					
					
//					let observable = doneButton.rx.tap
//						.takeUntil(viewController.rx.deallocated)
//						.flatMap({ [unowned dataSource] _ in
//							dataSource.bind(to: delegate)
////								.takeUntil(viewController.rx.deallocated)
//								.unwrap()
//								.take(1)
//						})
////						.unwrap()
//						.take(1)
////						.takeUntil(Observable.combineLatest(viewController.rx.deallocated, delegate.rx.deallocated))
//						.logOnDispose()
//
////					let observable = dataSource.bind(to: delegate)
////						//.logOnDispose()
////						.takeUntil(viewController.rx.deallocated)
////						.unwrap()
////						.take(1)
//
////					return Pair(viewController, observable
//
//
//					return Observable.just(viewController)
//						.flatMap({ [unowned dataSource, delegate] viewController in
//							dataSource.bind(to: delegate)
//								.unwrap()
//								.takeUntil(viewController.rx.deallocated)
//								.take(1)
//								.map({
//									(viewController, $0)
//								})
//						})
//						.takeUntil(viewController.rx.deallocated)
					
					
					
//					let observable = doneButton.rx.tap
//						.takeUntil(viewController.rx.deallocated)
//						.flatMap({ [unowned dataSource] _ in
//							dataSource.bind(to: delegate)
//								//								.takeUntil(viewController.rx.deallocated)
//								.unwrap()
//								.take(1)
//						})
//						//						.unwrap()
//						.take(1)
//						//						.takeUntil(Observable.combineLatest(viewController.rx.deallocated, delegate.rx.deallocated))
//						.logOnDispose()

//
//					let observable = dataSource.bind(to: delegate)
//						.unwrap()
//						.takeUntil(viewController.rx.deallocated)
//
//					_ = doneButton.rx.tap
//						.takeUntil(viewController.rx.deallocated)
//						.flatMap({ _ in
//							observable
//						})
//						.bind(onNext: { [unowned viewController] events in
//							block(viewController, events)
//						})
					
//					let observableA = doneButton.rx.tap.take(1)
//					let observableB = dataSource.bind(to: delegate)
//						.unwrap()
					
					_ = dataSource.bind(to: delegate).unwrap()
						.ignore(until: doneButton.rx.tap.take(1))
						.do(onNext: { [unowned viewController] events in
							dump(events.map({$0.title}))
							block(viewController, events)
							//viewController.dismiss(animated: true, completion: nil)
						})
						.takeUntil(viewController.rx.deallocated)
						.debug()
						.subscribe()
//					_ = Observable.or(observableA, observableB)
////					.takeUntil(viewController.rx.deallocated)
//						.do(onNext: { (pair: ExclusivePair<(), [EKEventCell.Model]>) in
//							switch pair {
//							case .A(let a):
//								block(viewController, a)
//							case .B(let b):
//								block(viewController, b)
//							}
//						})
////						.do(onNext: { pair in
////
////						})
////						.bind(onNext: { exclusivePair in
////
////						})
//						.takeUntil(viewController.rx.deallocated)
//						.subscribe()
					
					from.show(viewController, sender: nil)
				}
				
				
				
//				let viewController = SelectionViewController<EKSource, EKCalendarCell>({ viewController in
//
//					viewController.navigationItem.title = "Calendars"
//
//					EKEventStore.default.sources
//						.sorted(by: \EKSource.title.localizedLowercase, <)
//						.forEach({ source in
//							let calendars = source.calendars(for: .event)
//								.sorted(by: \EKCalendar.title.localizedLowercase, <)
//
//							viewController.dataProvider.append(section: source, rows: calendars)
//						})
//
//					viewController.dataProvider.sectionTitleProvider = {
//						$0.title
//					}
//
//				})
//
//				viewController.selection
//				.bind(onNext: {
//					print($0)
//				})
				
//				Observable.once(viewController)
//					.flatMapLatest({ viewController in
//						viewController.selection
//					})
				
//				navigationController.pushViewController(viewController, animated: true)
				
				
				
				
				
//				let viewController = SelectionViewController(
//					EKEventStore.default.sources
//						.sorted(by: \EKSource.title.localizedLowercase, <)
//						.reduce(into: UITableViewDataProvider<EKSource, EKCalendarCell>(), {
//							let source = $1
//							let calendars = source.calendars(for: .event)
//								.sorted(by: \EKCalendar.title.localizedLowercase, <)
//							$0.append(section: source, rows: calendars)
//						})
//				) {
//					$0.navigationItem.title = "Calendars"
//
//					$0.sectionTitleProvider = {
//						$0.title
//					}
//				}
//
//				_ = viewController.selection
//					.map({
//						Observable.just($0)
//					})
//					.flatMapLatest({ observable in
//						observable.map({ viewController, calendar in
//							SelectionViewController(
//								EKEventStore.default
//									.events(from: calendar)
//									.grouped(by: \EKEvent.eventIdentifier)
//									.compactMap({ $0.value.first })
//									.grouped(by: \EKEvent.startDate?.year)
//									.reduce(into:  UITableViewDataProvider<Int?, EKEventCell>(), {
//										$0.append(section: $1.key, rows: $1.value)
//									})
//							) {
//								$0.navigationItem.title = calendar.title
//
//								$0.sectionTitleProvider = {
//									guard let year = $0 else { return "!" }
//									return "\(year)"
//								}
//							}
//						})
//							.do(onNext: {
//								viewController.show($0, sender: nil)
//							})
//					})
//					.flatMapLatest({
//						$0.selection
//					})
//					.takeUntil(navigationController.rx.deallocated)
//					.bind(onNext: { viewController, event in
//						viewController.dismiss(animated: true, completion: nil)
//						print(event.title)
//					})
				
				calendarSelector(with: EKEventStore.default, from: navigationController, { viewController, calendar in
					eventSelector(with: calendar, from: viewController, { viewController, events in
						
						
//						let batch = Firestore.firestore().batch()
						
						defer {
//							batch.commit()
							viewController.dismiss(animated: true, completion: nil)
						}
						
//						guard let user = Auth.auth().currentUser else { return }
						
						for event in events {
							print(event)
//							user.document
//								.collectionOf(EventIdentifier.self)
//								.ref
//								.documentOf(EventIdentifier.self, event.eventIdentifier)
//								.setValue(EventIdentifier(event), batch)
						}
						
					})
				})
				
				//navigationController.pushViewController(viewController, animated: false)
				
//				Observable.just(navigationController)
//					.map({ navigationController in
//						(navigationController, calendarSelector(EKEventStore.default))
//					})
//					.flatMap({ navigationController, observable in
//						observable
//							.do(onNext: { viewController, selection in
//								navigationController.pushViewController(viewController, animated: true)
//							})
////							.map({ viewController, selection in
////								selection
////							})
//					})
//					.map({ viewController, selection in
//						(viewController, eventSelector(selection))
//					})
//					.flatMap({ viewController, observable in
//						observable
//							.do(onNext: { viewController, selection in
//								navigationController.pushViewController(viewController, animated: true)
//							})
//						//							.map({ viewController, selection in
//						//								selection
//						//							})
//					})
//					.map({ viewController, selection in
//						selection
//					})
////					.do(onNext: { navigationController, observable in
////						navigationController.pushViewController(pair.0, animated: true)
////					})
////					.map({ navigationController, pair in
////						(pair.0, pair.1)
////					})
////					.flatMap({ viewController, observable in
////						observable
////							.do(onNext: { viewController, selection in
////								viewController.show(viewController, animated: true)
////							})
////					})
////					.map({ viewController, selection in
////						selection
////					})
////					.do(onNext: { viewController, pair in
////						viewController.show(pair.0, sender: nil)
////					})
////					.map({ viewController, pair in
////						(pair.0, pair.1)
////					})
////					.flatMapLatest({ viewController, observable in
////						observable
////							.map({
////								(viewController, $0)
////							})
////					})
//					.take(1)
//					.logOnDispose()
//					.bind(onNext: { selection in
//
//						let batch = Firestore.firestore().batch()
//
//						defer {
//							batch.commit()
////							viewController.dismiss(animated: true, completion: nil)
//						}
//
//						guard let user = Auth.auth().currentUser else { return }
//
//						for event in selection {
//							user.document
//								.collectionOf(EventIdentifier.self)
//								.ref
//								.documentOf(EventIdentifier.self, event.eventIdentifier)
//								.setValue(EventIdentifier(event), batch)
//						}
//					})
				
				
				
				
//					.bind(onNext: { [unowned viewController] selection in
				
						//							}
//					})
//					.disposed(by: delegate.disposeBag)
				
				
				
				
//				navigationController.pushViewController(viewController, animated: false)
//				navigationController.pushViewController(viewController2, animated: false)
				
				self.show(navigationController, sender: nil)
			})
			.disposed(by: self.disposeBag)
		
	}
	
	override func viewDidAppear(_ animated: Bool)
	{
		super.viewDidAppear(animated)
	}
	
	override func viewWillDisappear(_ animated: Bool)
	{
		super.viewWillDisappear(animated)
		
		self.disposeBag = DisposeBag()
		
		/*
		self.batch.commitPromise()
			.catch({ error in
				print(error)
			})
		*/
	}
	
	override func viewDidDisappear(_ animated: Bool)
	{
		super.viewDidDisappear(animated)
	}
}





extension CountdownViewController//: UITableViewDelegate
{
	func tableView(_ tableView: UITableView,
				   leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
	{
		return nil
	}
	
//	func tableView(_ tableView: UITableView,
//				   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
//	{
//		guard let user = Auth.auth().currentUser else { return nil }
//		let field = user.document.fieldOf(EKEvents.self)
//		
//		let action = UIContextualAction(style: UIContextualAction.Style.destructive,
//										title: "Delete")
//		{ [unowned self] (action, view, handler) in
//			
//			var events = self.dataSource.events
//			events.remove(at: indexPath.row)
//			
//			
////			let element = self.dataSource.events[indexPath.row]
//			field.setValue(EKEvents(events))
//			
//			//			element.document.delete(self.batch)
//			//				.done({ _ in
//			handler(true)
//			//				}).catch({ error in
//			//					handler(false)
//			//				})
//		}
//		
//		return UISwipeActionsConfiguration(actions: [action])
//	}
}




