//
//  CalendarSelectorViewController.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2018-10-27.
//  Copyright © 2018 Pat Sluth. All rights reserved.
//

import UIKit
import EventKit

import RxSwift
import RxCocoa
import RxSwiftExt
import Permission




public protocol BlockConfigurable
{
	
}

public func configuree<T>(_ object: T, _ block: (inout T) -> Void)
{
	var object = object
	block(&object)
}

public extension BlockConfigurable
{
	public func configure(_ block: (inout Self) -> Void)
	{
		var `self` = self
		block(&self)
	}
}

extension NSObject: BlockConfigurable
{
	
}




