//
//  EKEventViewController.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2018-10-27.
//  Copyright © 2018 Pat Sluth. All rights reserved.
//

import UIKit
import EventKit
import NotificationCenter

import Firebase
import FirebaseAuth
import FirebaseFirestore
import RxSwift
import RxCocoa
import PromiseKit
import SwiftDate
import Permission





private let CalendarName = "CalendarApp"


//public typealias FirestoreCollectionViewControllerType = UIViewController & UITableViewDataSource & UITableViewDelegate
//public protocol FirestoreCollectionViewControllerProtocol: UITableViewDataSource & UITableViewDelegate
//	where Self: UIViewController
//{
//	associatedtype Model: Firestore.ModelType
//	associatedtype Cell: FirestoreCollectionCell<Model>
//}

public class FirestoreCollectionViewController<Model, Cell>: UIViewController, UITableViewDataSource, UITableViewDelegate, NCWidgetProviding
	where
	Model: Firestore.ModelType,
	Cell: UITableViewCell & ModelViewProtocol,
	Cell.Model == ValueResult<Model>
{
	@IBOutlet fileprivate var tableView: UITableView!
	
	fileprivate var dataSource = [DocumentSnapshot.Value<Model>]()
	
	var disposeBag = DisposeBag()
	
	
	
	
	
	public override func viewDidLoad()
	{
		super.viewDidLoad()
		
		self.tableView.register(Cell.nib, of: Cell.self)
		
		if let extensionContext = self.extensionContext {
			extensionContext.widgetLargestAvailableDisplayMode = NCWidgetDisplayMode.expanded
		} else {
			self.widgetPerformUpdate { _ in }
		}
	}
	
	public override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
		
		Auth.auth().rx.currentUser()
			.do(onNext: { user in
				self.dataSource.removeAll()
				self.tableView.reloadData()
			})
			.unwrap()
			.map({ user in
				user.document.collectionOf(Model.self)
			})
			.flatMapLatest({ collection in
				collection.observable(queryProvider: {
					$0.order(by: "\(EventIdentifier.CodingKeys.startDate.rawValue)")
				})
			})
			//			.filter({ !$0.isEmpty })
//			.logOnDispose()
			.bind(onNext: { elements in
				
				elements.forEach {
					let oldIP = IndexPath(row: Int($0.change.oldIndex), section: 0)
					let newIP = IndexPath(row: Int($0.change.newIndex), section: 0)
					
					switch $0.change.type {
					case .added:
						self.dataSource.insert($0.snapshot, at: newIP.row)
						self.tableView.insertRows(at: [newIP], with: .none)
					case .modified:
						self.dataSource.remove(at: oldIP.row)
						self.tableView.deleteRows(at: [oldIP], with: .none)
						self.dataSource.insert($0.snapshot, at: newIP.row)
						self.tableView.insertRows(at: [newIP], with: .none)
					case .removed:
						self.dataSource.remove(at: oldIP.row)
						self.tableView.deleteRows(at: [oldIP], with: .none)
					}
				}
				
				self.widgetUpdatePreferredContentSize()
			})
			.disposed(by: self.disposeBag)
	}
	
	public override func viewDidAppear(_ animated: Bool)
	{
		super.viewDidAppear(animated)
	}
	
	public override func viewWillDisappear(_ animated: Bool)
	{
		super.viewWillDisappear(animated)
		
		self.disposeBag = DisposeBag()
	}
	
	public override func viewDidDisappear(_ animated: Bool)
	{
		super.viewDidDisappear(animated)
	}
	
	
	
	
	
	// MARK: UITableViewDataSource
	
	public func numberOfSections(in tableView: UITableView) -> Int
	{
		return 1
	}
	
	public func tableView(_ tableView: UITableView,
				   numberOfRowsInSection section: Int) -> Int
	{
		return self.dataSource.count
	}
	
	public func tableView(_ tableView: UITableView,
				   cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		let cell = tableView.dequeueReusableCell(of: Cell.self, for: indexPath)
		let element = self.dataSource[indexPath.row]
		
		cell.update(with: element.result)
		//		switch element.result {
		//		case .Success(let value):
		//			cell.set(event: value.event)
		//		case .Failure(let error):
		//			print(error.localizedDescription)
		//		}
		
		
		
		//		_ = Observable<Int>.interval(1.0, scheduler: MainScheduler.instance)
		//			//			.onceThenOnRunLoop(inModes: [RunLoop.Mode.default])
		//			.bind(onNext: { [unowned cell] _ in
		//				cell.set(event: element)
		//			})
		//			.disposed(by: cell.disposeBag)
		
		return cell
	}
	
	
	
	
	
	// MARK: UITableViewDelegate
	
	public func tableView(_ tableView: UITableView,
				   estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
	{
		return tableView.estimatedRowHeight
	}
	
	public func tableView(_ tableView: UITableView,
				   heightForRowAt indexPath: IndexPath) -> CGFloat
	{
		return tableView.rowHeight
	}
	
	public func tableView(_ tableView: UITableView,
				   didEndDisplaying cell: UITableViewCell,
				   forRowAt indexPath: IndexPath)
	{
		cell.prepareForReuse()
	}
	
	public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		
	}
	
	//	func tableView(_ tableView: UITableView,
	//				   leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
	//	{
	//		let action = UIContextualAction (style: UIContextualAction.Style.normal, title: "Set Reminder")
	//		{   [unowned self] (action, view, handler) in
	//
	//			let element = self.dataSource[indexPath.row]
	//
	//			let viewController = R.storyboard.main.medicationReminderViewController()!
	//			viewController.valueResult = element.result
	//
	//			let navigationController = UINavigationController(rootViewController: viewController)
	//			self.present(navigationController, animated: true, completion: nil)
	//		}
	//
	//		return UISwipeActionsConfiguration(actions: [action])
	//	}
	
	public func tableView(_ tableView: UITableView,
						  trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
	{
		let action = UIContextualAction(style: UIContextualAction.Style.destructive,
										title: "Delete")
		{ [unowned self] (action, view, handler) in
			
			let element = self.dataSource[indexPath.row]
			element.document.delete()
				.done({ _ in
					handler(true)
				}).catch({ error in
					handler(false)
				})
		}
		
		return UISwipeActionsConfiguration(actions: [action])
	}
	
	
	
	
	
	// MARK: NCWidgetProviding
	
	public func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void))
	{
		//				let dateInterval = DateInterval(start: Date(), duration: TimeInterval.year)
		//				let dateInterval = DateInterval(start: Date.distantPast, end: Date())
		let dateInterval = DateInterval(start: Date(), duration: TimeInterval.year)
		
		Permission.events.onRequest(status: PermissionStatus.authorized)
			.then({ _ in
				EKEventStore.default.calendar(named: CalendarName, for: .event)
			})
			.then({ calendar in
				EKEventStore.default.eventsPromise(from: calendar, for: dateInterval)
			})
			.done({ events in
				//				self.dataSource = EKEvents(events
				//					.grouped(by: \EKEvent.eventIdentifier)
				//					.compactMap({ $0.value.first })
				//					.sorted(by: \EKEvent.startDate, <))
			})
			.catch({ error in
				//				self.dataSource = EKEvents()
			})
			.finally({
				completionHandler(NCUpdateResult.newData)
			})
	}
	
	public func widgetUpdatePreferredContentSize()
	{
		guard let extensionContext = self.extensionContext else { return }
		
		let maxSize = extensionContext.widgetMaximumSize(for: extensionContext.widgetActiveDisplayMode)
		self.widgetActiveDisplayModeDidChange(extensionContext.widgetActiveDisplayMode,
											  withMaximumSize: maxSize)
	}
	
	public func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode,
												 withMaximumSize maxSize: CGSize)
	{
		let width = maxSize.width
		let height = min(self.tableView.contentSize.height, maxSize.height)
		
		self.preferredContentSize = CGSize(width: width, height: height)
		self.view.setNeedsLayout()
		self.view.layoutIfNeeded()
	}
}

//public protocol FirestoreCollectionCellProtocol: UITableViewModelCellProtocol
//	where Model: Firestore.ModelType
//{
//	typealias Model = ValueResult<Model>
//}

//open class FirestoreCollectionCell<T>: UITableViewCellBase, UITableViewModelCellProtocol
//	where T: Firestore.ModelType
//{
//	public typealias Model = ValueResult<T>
//
//	public static var nib: UINib? {
//		return nil
//	}
//
//	open func update(with model: ValueResult<T>)
//	{
//
//	}
//}





class CountdownBaseViewController: FirestoreCollectionViewController<EventIdentifier, CountdownCell2>
{
//	@IBOutlet fileprivate var tableView: UITableView!
	
	private let calendarComponents = [Calendar.Component.year,
		Calendar.Component.month,
		Calendar.Component.weekOfMonth,
		Calendar.Component.day,
		Calendar.Component.hour,
		Calendar.Component.minute,
		Calendar.Component.second
	]
	
	//private(set)
//	var dataSource = EKEvents() {
//		didSet
//		{
//			self.tableView.reloadData()
//			self.view.setNeedsLayout()
//			self.view.layoutIfNeeded()
//			self.updatePreferredContentSize()
//		}
//	}
	
//	private var dataSource = [EKEvent]() {
//	private var dataSource = [DocumentSnapshot.Value<EventIdentifier>]() //{
//		didSet
//		{
//			self.tableView.reloadData()
//			self.view.setNeedsLayout()
//			self.view.layoutIfNeeded()
//			self.updatePreferredContentSize()
//		}
//	}
	
//	var disposeBag = DisposeBag()
	
	
	
	
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		self.tableView.register(UINib(resource: R.nib.countdownCell2), of: CountdownCell2.self)
		
//		if let extensionContext = self.extensionContext {
//			extensionContext.widgetLargestAvailableDisplayMode = NCWidgetDisplayMode.expanded
//		} else {
//			self.widgetPerformUpdate { _ in }
//		}
	}
	
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
		
//		Auth.auth().rx.currentUser()
//			.do(onNext: { user in
//				self.dataSource.removeAll()
//				self.tableView.reloadData()
//			})
//			.unwrap()
//			.map({ user in
//				user.document.collectionOf(EventIdentifier.self)
//			})
//			.flatMapLatest({ collection in
//				collection.observable(queryProvider: {
//					$0.order(by: EventIdentifier.CodingKeys.startDate.rawValue)
//				})
//			})
////			.filter({ !$0.isEmpty })
//			.logOnDispose()
//			.bind(onNext: { elements in
//
//				elements.forEach {
//					let oldIP = IndexPath(row: Int($0.change.oldIndex), section: 0)
//					let newIP = IndexPath(row: Int($0.change.newIndex), section: 0)
//
//					switch $0.change.type {
//					case .added:
//						self.dataSource.insert($0.snapshot, at: newIP.row)
//						self.tableView.insertRows(at: [newIP], with: .none)
//					case .modified:
//						self.dataSource.remove(at: oldIP.row)
//						self.dataSource.insert($0.snapshot, at: newIP.row)
//						self.tableView.deleteRows(at: [oldIP], with: .none)
//						self.tableView.insertRows(at: [newIP], with: .none)
//					case .removed:
//						self.dataSource.remove(at: oldIP.row)
//						self.tableView.deleteRows(at: [oldIP], with: .none)
//					}
//				}
//
//				self.updatePreferredContentSize()
//			})
//			.disposed(by: self.disposeBag)
	}
	
	override func viewDidAppear(_ animated: Bool)
	{
		super.viewDidAppear(animated)
	}
	
	override func viewWillDisappear(_ animated: Bool)
	{
		super.viewWillDisappear(animated)
	}
	
	override func viewDidDisappear(_ animated: Bool)
	{
		super.viewDidDisappear(animated)
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		let element = self.dataSource[indexPath.row]
		
		var event = element.result.value!.event
		print(event.startDate)
		
		if let e = EKEventStore.default.events(with: event.eventIdentifier)
			.filter({ $0.startDate > Date() })
			.sorted(by: \EKEvent.startDate, <)
			.first {
			
			print(e == event)
			print(event.startDate)
			print(e.startDate)
			if e != event {
				let x = EventIdentifier(e)
				element.document.setValue(x)
				.done({ _ in
					print("DONE")
				}).catch { (e) in
					print( e)
				}
			}
			
		}
		//			.first(where: {
		//				$0.title == title && $0.startDate == startDate
		//			}) {
	}
}





//extension CountdownBaseViewController: UITableViewDataSource
//{
//	func numberOfSections(in tableView: UITableView) -> Int
//	{
//		return 1
//	}
//
//	func tableView(_ tableView: UITableView,
//				   numberOfRowsInSection section: Int) -> Int
//	{
//		return self.dataSource.count
//	}
//
//	func tableView(_ tableView: UITableView,
//				   cellForRowAt indexPath: IndexPath) -> UITableViewCell
//	{
//		let cell = tableView.dequeueReusableCell(of: CountdownCell2.self, for: indexPath)
//		let element = self.dataSource[indexPath.row]
//
//		cell.update(with: element.result)
////		switch element.result {
////		case .Success(let value):
////			cell.set(event: value.event)
////		case .Failure(let error):
////			print(error.localizedDescription)
////		}
//
//
//
////		_ = Observable<Int>.interval(1.0, scheduler: MainScheduler.instance)
////			//			.onceThenOnRunLoop(inModes: [RunLoop.Mode.default])
////			.bind(onNext: { [unowned cell] _ in
////				cell.set(event: element)
////			})
////			.disposed(by: cell.disposeBag)
//
//		return cell
//	}
//}
//
//
//
//
//
//extension CountdownBaseViewController: UITableViewDelegate
//{
//	func tableView(_ tableView: UITableView,
//				   estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
//	{
//		return tableView.estimatedRowHeight
//	}
//
//	func tableView(_ tableView: UITableView,
//				   heightForRowAt indexPath: IndexPath) -> CGFloat
//	{
//		return tableView.rowHeight
//	}
//
//	func tableView(_ tableView: UITableView,
//				   didEndDisplaying cell: UITableViewCell,
//				   forRowAt indexPath: IndexPath)
//	{
//		cell.prepareForReuse()
//	}
//
//	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
//	{
//
//	}
//
////	func tableView(_ tableView: UITableView,
////				   leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
////	{
////		let action = UIContextualAction (style: UIContextualAction.Style.normal, title: "Set Reminder")
////		{   [unowned self] (action, view, handler) in
////
////			let element = self.dataSource[indexPath.row]
////
////			let viewController = R.storyboard.main.medicationReminderViewController()!
////			viewController.valueResult = element.result
////
////			let navigationController = UINavigationController(rootViewController: viewController)
////			self.present(navigationController, animated: true, completion: nil)
////		}
////
////		return UISwipeActionsConfiguration(actions: [action])
////	}
//
//	func tableView(_ tableView: UITableView,
//				   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
//	{
//		let action = UIContextualAction(style: UIContextualAction.Style.destructive,
//										title: "Delete")
//		{ [unowned self] (action, view, handler) in
//
//			let element = self.dataSource[indexPath.row]
//			element.document.delete()
//				.done({ _ in
//					handler(true)
//				}).catch({ error in
//					handler(false)
//				})
//		}
//
//		return UISwipeActionsConfiguration(actions: [action])
//	}
//}





//extension CountdownBaseViewController: NCWidgetProviding
//{
//	func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void))
//	{
//		//				let dateInterval = DateInterval(start: Date(), duration: TimeInterval.year)
//		//				let dateInterval = DateInterval(start: Date.distantPast, end: Date())
//		let dateInterval = DateInterval(start: Date(), duration: TimeInterval.year)
//
//		Permission.events.onRequest(status: PermissionStatus.authorized)
//			.then({ _ in
//				EKEventStore.default.calendar(named: CalendarName, for: .event)
//			})
//			.then({ calendar in
//				EKEventStore.default.eventsPromise(from: calendar, for: dateInterval)
//			})
//			.done({ events in
////				self.dataSource = EKEvents(events
////					.grouped(by: \EKEvent.eventIdentifier)
////					.compactMap({ $0.value.first })
////					.sorted(by: \EKEvent.startDate, <))
//			})
//			.catch({ error in
////				self.dataSource = EKEvents()
//			})
//			.finally({
//				completionHandler(NCUpdateResult.newData)
//			})
//	}
//
//	fileprivate func updatePreferredContentSize()
//	{
//		guard let extensionContext = self.extensionContext else { return }
//
//		let maxSize = extensionContext.widgetMaximumSize(for: extensionContext.widgetActiveDisplayMode)
//		self.widgetActiveDisplayModeDidChange(extensionContext.widgetActiveDisplayMode,
//											  withMaximumSize: maxSize)
//	}
//
//	func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode,
//										  withMaximumSize maxSize: CGSize)
//	{
//		let width = maxSize.width
//		let height = min(self.tableView.contentSize.height, maxSize.height)
//
//		//		for section in (0..<self.tableView.numberOfSections) {
//		//			for row in (0..<self.tableView.numberOfRows(inSection: section)) {
//		//				let indexPath = IndexPath(row: row, section: section)
//		//				let cell = self.tableView.cellForRow(at: indexPath)
//		//				print(section, row, cell?.bounds)
//		//			}
//		//		}
//		//		self.tableView.numberOfSections
//		//		for cell in self.tableView.indexDisplayMode {
//		//			print(cell.bounds.height)
//		//		}
//		//
//		self.preferredContentSize = CGSize(width: width, height: height)
//		self.view.setNeedsLayout()
//		self.view.layoutIfNeeded()
//	}
//}




