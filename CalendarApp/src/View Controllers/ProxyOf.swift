//
//  ProxyOf.swift
//  Sluthware
//
//  Created by Pat Sluth on 2018-10-27.
//  Copyright © 2018 Pat Sluth. All rights reserved.
//

import RxSwift
import RxCocoa



// TODO: move to Sluthware
//open class UnretainedObject<O>: NSObject
//	where O: AnyObject
//{
//	private var _object: Unmanaged<O>? = nil
//	weak var object: O? {
//		get
//		{
//			print(self)
//			return self._object?.takeUnretainedValue()
//		}
//		set
//		{
//			if let object = newValue {
//				self._object = Unmanaged<O>.passUnretained(object)
//			} else {
//				self._object = nil
//			}
//		}
//	}
//
//
//
//
//
//	required public override init()
//	{
//		super.init()
//	}
//
//	convenience init(_ object: O?)
//	{
//		self.init()
//
//		defer {
//			self.object = object
//		}
//	}
//
//	deinit
//	{
//		print(type(of: self), #function)
//	}
//}
//
//
//
//
//
//open class ProxyOf<Base>: UnretainedObject<Base>
//	where Base: AnyObject
//{
//	var base: Base? {
//		get { return self.object }
//		set { self.object = newValue }
//	}
//}


//extension ProxyOf: BlockInitializable
//{
//	
//}

open class ProxyOf<Base>: NSObject
	where Base: AnyObject
{
	weak var base: Base? = nil
	public let disposeBag = DisposeBag()
	
	
	
	
	
	required public override init()
	{
		super.init()
	}
//
//	convenience public init(_ base: Base?)
//	{
//		self.init()
//		
//		self.base = base
//	}
	
	deinit
	{
		print(type(of: self), #function)
	}
}



