
//
//  CalendarSelectorViewController.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2018-10-27.
//  Copyright © 2018 Pat Sluth. All rights reserved.
//

import UIKit
import EventKit

import RxSwift
import RxCocoa
import RxSwiftExt
import Permission






extension UIViewController
{
	func barButton(systemItem: UIBarButtonItem.SystemItem, _ block: (UIBarButtonItem) -> Void) -> Observable<()>
	{
		let barButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)
		
		block(barButtonItem)
		
		return barButtonItem.rx.tap
			.takeUntil(barButtonItem.rx.deallocated)
			.debug()
	}
}


//class DataTester<DataProvider>
//	where DataProvider: UITableViewDataProviderProtocol
//{
//
//
//	init(dataProviderType type: DataProvider.Type)
//	{
//		let d = type(tableView: x)
//	}
//}
//protocol SelectionType
//{
//	associatedtype X
//}
//
//struct SingleSelection<T>: SelectionType
//{
//	typealias X = T
//}


class DataProviderViewController3: UIViewController, UITableViewDelegate
{
	//	typealias DataProviderType = DataProvider.Type
	//	var
	
//	func tableView(_ tableView: UITableView,
//				   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
//	{
//		let action = UIContextualAction(style: UIContextualAction.Style.destructive,
//										title: "TEST")
//		{ [unowned self] (action, view, handler) in
//		}
//
//		return UISwipeActionsConfiguration(actions: [action])
//	}
	
	
	lazy var tableView: UITableView = {
		let tableView = UITableView()
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.delegate = self
		return tableView
	}()
//	//	let dataProvider: DataProvider
//	lazy var dataProvider: DataProvider = {
//		//		return DataProvider(tableView: self.tableView)
//		return DataProvider(tableView: self.tableView)
//	}()
	
	//	var sectionTitleProvider: ((Section) -> String?)! = nil
	//	var cellConfigure: ((Cell) -> Void)! = nil
	//var valueSelected: ((Cell.Value?) -> UIViewController?)!
	//	var viewControllerForOutput: ((Cell.Value) -> UIViewController?)!
	
	deinit
	{
		print(type(of: self), #function)
	}
	
	//var type: DataProvider.Type!
//
//	convenience init(dataProviderType type: DataProvider.Type)
//	{
//		//		type(self.tableView)
//		//		T(self.tableView)
//		//		self.type = type
//		self.init()
//	}
	
	//	convenience init(_ initBlock: (DataProviderViewController2) -> Void)
	//	{
	//		self.init()
	//
	//		initBlock(self)
	//	}
	
	//	required init?(coder aDecoder: NSCoder)
	//	{
	//		fatalError("init(coder:) has not been implemented")
	//	}
	
	open override func loadView()
	{
		self.view = {
			let view = UIView()
			view.backgroundColor = UIColor.white
			return view
		}()
		
		self.view.addSubview(self.tableView)
		
		NSLayoutConstraint.activate([
			self.tableView.topAnchor.constraint(equalTo: self.view.safeTopAnchor),
			self.tableView.leftAnchor.constraint(equalTo: self.view.safeLeftAnchor),
			self.tableView.bottomAnchor.constraint(equalTo: self.view.safeBottomAnchor),
			self.tableView.rightAnchor.constraint(equalTo: self.view.safeRightAnchor),
			])
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
//		let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)
//		
//		self.navigationItem.rightBarButtonItem = doneButton
//		
//		_ = doneButton.rx.tap
//			.takeUntil(self.rx.deallocated)
//			.bind(onNext: { [unowned self] in
//				self.dismiss(animated: true, completion: nil)
//			})
	}
	
	//	override func viewWillAppear(_ animated: Bool)
	//	{
	//		super.viewWillAppear(animated)
	//
	//
	//	}
	
	
	
	
	
	// MARRK: UITableViewDataSource
	
	//	open func numberOfSections(in tableView: UITableView) -> Int
	//	{
	//		return self.dataProvider.numberOfSections()
	//	}
	//
	//	open func tableView(_ tableView: UITableView,
	//						titleForHeaderInSection section: Int) -> String?
	//	{
	//		let section = self.dataProvider.section(at: section)
	//		return self.sectionTitleProvider?(section.0)
	//	}
	//
	//	open func tableView(_ tableView: UITableView,
	//						numberOfRowsInSection section: Int) -> Int
	//	{
	//		return self.dataProvider.numberOfRows(in: section)
	//	}
	//
	//	open func tableView(_ tableView: UITableView,
	//						cellForRowAt indexPath: IndexPath) -> UITableViewCell
	//	{
	//		return self.dataProvider.tableView(tableView, cellForRowAt: indexPath)
	//	}
	
	
	
	// MARK: UITableViewDelegate
	
	func tableView(_ tableView: UITableView,
				   estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
	{
		return tableView.estimatedRowHeight
	}
	
	func tableView(_ tableView: UITableView,
				   heightForRowAt indexPath: IndexPath) -> CGFloat
	{
		return tableView.rowHeight
	}
	
	func tableView(_ tableView: UITableView,
				   willDisplay cell: UITableViewCell,
				   forRowAt indexPath: IndexPath)
	{
//		print(#file.fileName, #function)
	}
	
	func tableView(_ tableView: UITableView,
				   didEndDisplaying cell: UITableViewCell,
				   forRowAt indexPath: IndexPath)
	{
//		print(#file.fileName, #function)
		cell.prepareForReuse()
	}
	
	func tableView(_ tableView: UITableView,
				   didDeselectRowAt indexPath: IndexPath)
	{
		
	}
	
	func tableView(_ tableView: UITableView,
				   didSelectRowAt indexPath: IndexPath)
	{
	}
}










class DataProviderViewController2<DataProvider>: UIViewController, UITableViewDelegate
	where DataProvider: AnyUITableViewDataProvider
{
//	typealias DataProviderType = DataProvider.Type
//	var
	
	func tableView(_ tableView: UITableView,
						  trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
	{
		let action = UIContextualAction(style: UIContextualAction.Style.destructive,
										title: "TEST")
		{ [unowned self] (action, view, handler) in
		}
		
		return UISwipeActionsConfiguration(actions: [action])
	}
	
	
	lazy var tableView: UITableView = {
		let tableView = UITableView()
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.delegate = self
		return tableView
	}()
//	let dataProvider: DataProvider
	lazy var dataProvider: DataProvider = {
//		return DataProvider(tableView: self.tableView)
		return DataProvider(tableView: self.tableView)
	}()
	
//	var sectionTitleProvider: ((Section) -> String?)! = nil
//	var cellConfigure: ((Cell) -> Void)! = nil
	//var valueSelected: ((Cell.Value?) -> UIViewController?)!
//	var viewControllerForOutput: ((Cell.Value) -> UIViewController?)!
	
	deinit
	{
		print(type(of: self), #function)
	}
	
	//var type: DataProvider.Type!
	
	convenience init(dataProviderType type: DataProvider.Type)
	{
//		type(self.tableView)
//		T(self.tableView)
//		self.type = type
		self.init()
	}
	
//	convenience init(_ initBlock: (DataProviderViewController2) -> Void)
//	{
//		self.init()
//
//		initBlock(self)
//	}
	
//	required init?(coder aDecoder: NSCoder)
//	{
//		fatalError("init(coder:) has not been implemented")
//	}
	
	open override func loadView()
	{
		self.view = {
			let view = UIView()
			view.backgroundColor = UIColor.white
			return view
		}()
		
		self.view.addSubview(self.tableView)
		
		NSLayoutConstraint.activate([
			self.tableView.topAnchor.constraint(equalTo: self.view.safeTopAnchor),
			self.tableView.leftAnchor.constraint(equalTo: self.view.safeLeftAnchor),
			self.tableView.bottomAnchor.constraint(equalTo: self.view.safeBottomAnchor),
			self.tableView.rightAnchor.constraint(equalTo: self.view.safeRightAnchor),
			])
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)

		self.navigationItem.rightBarButtonItem = doneButton

		_ = doneButton.rx.tap
			.takeUntil(self.rx.deallocated)
			.bind(onNext: { [unowned self] in
				self.dismiss(animated: true, completion: nil)
			})
	}
	
//	override func viewWillAppear(_ animated: Bool)
//	{
//		super.viewWillAppear(animated)
//
//
//	}
	
	
	
	
	
	// MARRK: UITableViewDataSource
	
//	open func numberOfSections(in tableView: UITableView) -> Int
//	{
//		return self.dataProvider.numberOfSections()
//	}
//
//	open func tableView(_ tableView: UITableView,
//						titleForHeaderInSection section: Int) -> String?
//	{
//		let section = self.dataProvider.section(at: section)
//		return self.sectionTitleProvider?(section.0)
//	}
//
//	open func tableView(_ tableView: UITableView,
//						numberOfRowsInSection section: Int) -> Int
//	{
//		return self.dataProvider.numberOfRows(in: section)
//	}
//
//	open func tableView(_ tableView: UITableView,
//						cellForRowAt indexPath: IndexPath) -> UITableViewCell
//	{
//		return self.dataProvider.tableView(tableView, cellForRowAt: indexPath)
//	}
	
	
	
	// MARK: UITableViewDelegate
	
	func tableView(_ tableView: UITableView,
						estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
	{
		return tableView.estimatedRowHeight
	}
	
	func tableView(_ tableView: UITableView,
						heightForRowAt indexPath: IndexPath) -> CGFloat
	{
		return tableView.rowHeight
	}
	
	func tableView(_ tableView: UITableView,
						willDisplay cell: UITableViewCell,
						forRowAt indexPath: IndexPath)
	{
		
	}
	
	func tableView(_ tableView: UITableView,
						didEndDisplaying cell: UITableViewCell,
						forRowAt indexPath: IndexPath)
	{
//		print(#file.fileName, #function)
		cell.prepareForReuse()
	}
	
	func tableView(_ tableView: UITableView,
						didDeselectRowAt indexPath: IndexPath)
	{
		
	}
	
	func tableView(_ tableView: UITableView,
						didSelectRowAt indexPath: IndexPath)
	{
	}
}







class DataProviderViewController<Section, Cell>: UIViewController, UITableViewDelegate
	where
	Cell: UITableView.ModelCell
{
	typealias DataProvider = UITableViewDataProvider<Section, Cell>
	
	
	
	
	
	lazy var tableView: UITableView = {
		let tableView = UITableView()
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.delegate = self
		return tableView
	}()
	
	lazy var dataProvider: DataProvider = {
		return DataProvider(tableView: self.tableView)
//		return DataProvider()
	}()
	
	deinit
	{
		print(type(of: self), #function)
	}
	
	convenience init(_ initBlock: (DataProviderViewController) -> Void)
	{
		self.init()
		
		initBlock(self)
	}
	
	//	convenience init(_ dataProvider: DataProvider, _ initBlock: (SelectionViewController) -> Void)
	//	{
	//		self.init(dataProvider)
	//
	//		initBlock(self)
	//	}
	
	//	open class func `init`(dataProvider: DataProvider) -> Self//, _ selectionBlock: (Cell.Value) -> Void) -> Self
	//	{
	//		let `self` = self.init()
	//
	//		self.input = input
	//		//self.output = BehaviorRelay<Cell.Value?>(value: nil)
	//		self.dataProvider = DataProvider()
	//
	//		return self
	//	}
	
	//	required public init?(coder aDecoder: NSCoder)
	//	{
	//		super.init(coder: aDecoder)
	//	}
	//
	//	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
	//	{
	//		super.init(nibName: nil, bundle: nil)
	//	}
	
	open override func loadView()
	{
		//		super.loadView()
		
		self.view = {
			let view = UIView()
			view.backgroundColor = UIColor.white
			return view
		}()
		
		self.view.addSubview(self.tableView)
		
		NSLayoutConstraint.activate([
			self.tableView.topAnchor.constraint(equalTo: self.view.safeTopAnchor),
			self.tableView.leftAnchor.constraint(equalTo: self.view.safeLeftAnchor),
			self.tableView.bottomAnchor.constraint(equalTo: self.view.safeBottomAnchor),
			self.tableView.rightAnchor.constraint(equalTo: self.view.safeRightAnchor),
			])
	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		
		
		//		self.tableView.register(DataProvider.Cell.nib(), of: DataProvider.Cell.self)
		
		
		
		//		let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)
		//
		//		self.navigationItem.rightBarButtonItem = doneButton
		//
		//		_ = doneButton.rx.tap
		//			.takeUntil(self.rx.deallocated)
		//			.bind(onNext: { [unowned self] in
		//				//				self.dismiss(animated: true, completion: nil)
		//				//				self.presentingViewController?.dismiss(animated: true, completion: nil)
		////				self._selection.accept(nil)
		//				//				self.presentingViewController?.dismiss(animated: true, completion: nil)
		//			})
	}
	
	
	
	// MARRK: UITableViewDataSource
	
	//	open func numberOfSections(in tableView: UITableView) -> Int
	//	{
	//		return self.dataProvider.numberOfSections()
	//	}
	//
	//	open func tableView(_ tableView: UITableView,
	//						titleForHeaderInSection section: Int) -> String?
	//	{
	//		let section = self.dataProvider.section(at: section)
	//		return self.sectionTitleProvider?(section.0)
	//	}
	//
	//	open func tableView(_ tableView: UITableView,
	//						numberOfRowsInSection section: Int) -> Int
	//	{
	//		return self.dataProvider.numberOfRows(in: section)
	//	}
	//
	//	open func tableView(_ tableView: UITableView,
	//						cellForRowAt indexPath: IndexPath) -> UITableViewCell
	//	{
	//		return self.dataProvider.tableView(tableView, cellForRowAt: indexPath)
	//	}
	
	
	
	// MARK: UITableViewDelegate
	
	open func tableView(_ tableView: UITableView,
						estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
	{
		return tableView.estimatedRowHeight
	}
	
	open func tableView(_ tableView: UITableView,
						heightForRowAt indexPath: IndexPath) -> CGFloat
	{
		return tableView.rowHeight
	}
	
	open func tableView(_ tableView: UITableView,
						willDisplay cell: UITableViewCell,
						forRowAt indexPath: IndexPath)
	{
		
	}
	
	open func tableView(_ tableView: UITableView,
						didEndDisplaying cell: UITableViewCell,
						forRowAt indexPath: IndexPath)
	{
		cell.prepareForReuse()
	}
	
	open func tableView(_ tableView: UITableView,
						didDeselectRowAt indexPath: IndexPath)
	{
		
	}
	
	open func tableView(_ tableView: UITableView,
						didSelectRowAt indexPath: IndexPath)
	{
	}
}







//protocol AnyStep: NSObjectProtocol
//{
//	func performStep()
//}
//
//
//class PartialStep<Input>
//{
//
//}
//
//class Step<Input, Output>: PartialStep<Input>
//{
//
//}
//
//
//class StepController
//{
////	typealias NextStepProvider<Input, Output> = (Input) -> SelectionViewController<Input, Output>
//	typealias NextStepProvider<Input> = (StepController, Input)
//
//	let navigationController: UINavigationController
//
//	init(navigationController: UINavigationController)
//	{
//		self.navigationController = navigationController
//	}
//
////	func start<Input>(with input: Input) -> Observable<NextStepProvider<Input>>
////	{
////		return self.add(input: input)
//////		return Observable.just(NextStepProvider(self, input))
//////			.do(onNext: { [unowned self] output in
//////				guard let output = output else { return }
//////				self.navigationController.dismiss(animated: true, completion: nil)
//////			})
//////			.unwrap()
//////			.map({ [unowned self] output -> Observable<ObservableType<O>> in
//////				Observable.just((stepController: self, output: output))
//////			})
////	}
////
////	private func add<Input>(input: Input) -> Observable<NextStepProvider<Input>>
////	{
////		return Observable.just(NextStepProvider(self, input))
////			.takeUntil(self.navigationController.rx.deallocated)
////		//		return Observable.just(NextStepProvider(self, input))
////		//			.do(onNext: { [unowned self] output in
////		//				guard let output = output else { return }
////		//				self.navigationController.dismiss(animated: true, completion: nil)
////		//			})
////		//			.unwrap()
////		//			.map({ [unowned self] output -> Observable<ObservableType<O>> in
////		//				Observable.just((stepController: self, output: output))
////		//			})
////	}
////
////	func addStepForInput<Input, Cell>(input: Input) -> Observable<SelectionViewController<Input, Cell>>
////		where Cell: UITableViewDataProviderCell
////	{
////		return Observable.just(NextStepProvider(self, input))
////			.takeUntil(self.navigationController.rx.deallocated)
////		//		return Observable.just(NextStepProvider(self, input))
////		//			.do(onNext: { [unowned self] output in
////		//				guard let output = output else { return }
////		//				self.navigationController.dismiss(animated: true, completion: nil)
////		//			})
////		//			.unwrap()
////		//			.map({ [unowned self] output -> Observable<ObservableType<O>> in
////		//				Observable.just((stepController: self, output: output))
////		//			})
////	}
////
////	typealias NextStepBlock<Output, Cell> = (Output) -> SelectionViewController<Output, Cell>
////		where Cell: UITableViewDataProviderCell
//
//	func addStep<A, B>(viewController: SelectionViewController<A, B>, onComplete: (StepController, B.Value) -> Void)
//		where B: UITableViewDataProviderCell
//	{
//		self.navigationController.pushViewController(viewController, animated: true)
//
//		_ = Observable.create({ [unowned self] observable in
//
//			let disposable = viewController.selection
//				.do(onNext: { [unowned self] output in
//					if output == nil {
//						self.navigationController.dismiss(animated: true, completion: {
//							observable.onCompleted()
//						})
//					}
//				})
//				.unwrap()
//				.flatMapLatest({ output in
//					self.add(input: output)
//				})
//				.bind(onNext: {
//					observable.onNext($0)
//				})
//			//				.flatMapLatest({ [unowned self] output in
//			//					self.start(with: output)
//			//				})
//			//				.takeUntil(viewController.rx.deallocated)
//			//				.bind(onNext: {
//			//
//			//				})
//
//			return Disposables.create {
//				disposable.dispose()
//			}
//		})
//		.takeUntil(viewController.rx.deallocated)
//		.subscribe()
//
////		return viewController.selection
////			.do(onNext: { [unowned self] output in
////				if output == nil {
////					self.navigationController.dismiss(animated: true, completion: nil)
////				}
////			})
////			.unwrap()
////			.flatMapLatest({ [unowned self] output in
////				self.start(with: output)
////			})
////			.takeUntil(viewController.rx.deallocated)
////			.bind(onNext: { [unowned self] output in
////
////			})
////		return Observable.just(NextStepProvider(self, viewController.selection))
////			.flatMapLatest({
////
////			})
////			.takeUntil(viewController.rx.deallocated)
//
//
////		return viewController.selection
////			.flatMapLatest({ [unowned self] input in
////				Observable.just(NextStepProvider(self, input))
////			})
////			.takeUntil(viewController.rx.deallocated)
//
////		return Observable.create { [unowned self] observable in
////
////			viewController.selection.map({
////				Observable.just(NextStepProvider(self, output))
////			})
////
////		}.takeUntil(viewController.rx.deallocated)
////
////		return viewController.selection
////			.do(onNext: { [unowned self] output in
////				guard let output = output else { return }
////				self.navigationController.dismiss(animated: true, completion: nil)
////			})
////			.unwrap()
////			.map({ [unowned self] output in
////				Observable.just(NextStepProvider(self, output))
////			})
//	}
//}


//extension UIViewController
//{
//	convenience init<Input, Output>(dataProvider: UITableViewDataProvider<Input, Output>)
//	{
//		self.init()
//
//		self.dataProvider = DataProvider()
//	}
//}

//class DataProviderViewController<Section, Row, Cell>: UIViewController
//	where Cell: UITableViewDataProviderCell, Row == Cell.Value
//{
//	typealias DataProvider = UITableViewDataProvider<Section, Cell>
//	var dataProvider: DataProvider!
////
//	convenience init(dataProvider: DataProvider)
//	{
//		self.init()
//
//		self.dataProvider = dataProvider
//	}
//}





class SelectionViewController<Section, Cell>: DataProviderViewController<Section, Cell>
	where Cell: UITableView.ModelCell
{
//	var selectionTest = ((Cell.Model) -> Void)? = nil
	
	private let _selection = BehaviorRelay<Cell.Model?>(value: nil)
	var selection: Observable<Cell.Model> {
		return self._selection.skip(1)
			.asObservable()
			.do(onNext: { [unowned self] in
				if $0 == nil {
					self.dismiss(animated: true, completion: nil)
				}
			})
			.unwrap()
//			.map({ [unowned self] in
//				(self, $0)
//			})
	}
	
//	override init(_ initBlock: (DataProviderViewController) -> Void)
//	{
//		self.init()
//
//		initBlock(self)
//	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		
		
		let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)
		
		self.navigationItem.rightBarButtonItem = doneButton
		
		_ = doneButton.rx.tap
			.takeUntil(self.rx.deallocated)
			.bind(onNext: { [unowned self] in
//				self.dismiss(animated: true, completion: nil)
//				self.presentingViewController?.dismiss(animated: true, completion: nil)
				self._selection.accept(nil)
				//				self.presentingViewController?.dismiss(animated: true, completion: nil)
			})
	}
	
	override func tableView(_ tableView: UITableView,
							didSelectRowAt indexPath: IndexPath)
	{
		let value = self.dataProvider.row(at: indexPath)
		
		self._selection.accept(value)
	}
}








class MultipleSelectionViewController<Section, Cell>: DataProviderViewController<Section, Cell>
	where Cell: UITableView.ModelCell
{
	private let _selection = BehaviorRelay<[Cell.Model]?>(value: nil)
	var selection: Observable<[Cell.Model]> {
		return self._selection.skip(1)
			.asObservable()
			.do(onNext: { [unowned self] in
				if $0 == nil {
					self.dismiss(animated: true, completion: nil)
				}
			})
			.unwrap()
		//			.map({ [unowned self] in
		//				(self, $0)
		//			})
	}
	
	//	override init(_ initBlock: (DataProviderViewController) -> Void)
	//	{
	//		self.init()
	//
	//		initBlock(self)
	//	}
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		self.tableView.allowsMultipleSelection = true
		
		
		
		let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: nil)
		
		self.navigationItem.rightBarButtonItem = doneButton
		
		_ = doneButton.rx.tap
			.takeUntil(self.rx.deallocated)
			.bind(onNext: { [unowned self] in
				let value = self.tableView.indexPathsForSelectedRows?.map({
					self.dataProvider.row(at: $0)
				})
				
				self._selection.accept(value)
			})
	}
	
	override func tableView(_ tableView: UITableView,
							willDisplay cell: UITableViewCell,
							forRowAt indexPath: IndexPath)
	{
		super.tableView(tableView, willDisplay: cell, forRowAt: indexPath)

		if tableView.indexPathsForSelectedRows?.contains(indexPath) ?? false {
			cell.accessoryType = UITableViewCell.AccessoryType.checkmark
		} else {
			cell.accessoryType = UITableViewCell.AccessoryType.none
		}
	}
	
	override func tableView(_ tableView: UITableView,
							didEndDisplaying cell: UITableViewCell,
							forRowAt indexPath: IndexPath)
	{
		super.tableView(tableView, didEndDisplaying: cell, forRowAt: indexPath)

		cell.accessoryType = UITableViewCell.AccessoryType.none
	}
	
	override func tableView(_ tableView: UITableView,
							didDeselectRowAt indexPath: IndexPath)
	{
		super.tableView(tableView, didDeselectRowAt: indexPath)
		
		let cell = tableView.cellForRow(at: indexPath)
		cell?.accessoryType = UITableViewCell.AccessoryType.none
	}
	
	override func tableView(_ tableView: UITableView,
							didSelectRowAt indexPath: IndexPath)
	{
		super.tableView(tableView, didSelectRowAt: indexPath)
		
		let cell = tableView.cellForRow(at: indexPath)
		cell?.accessoryType = UITableViewCell.AccessoryType.checkmark
	}
}



//let viewController = SelectionViewController<EKSource, EKCalendarCell>({ viewController in
//
//	viewController.navigationItem.title = "Calendars"
//
//	EKEventStore.default.sources
//		.sorted(by: \EKSource.title.localizedLowercase, <)
//		.forEach({ source in
//			let calendars = source.calendars(for: .event)
//				.sorted(by: \EKCalendar.title.localizedLowercase, <)
//
//			viewController.dataProvider.append(section: source, rows: calendars)
//		})
//
//	viewController.dataProvider.sectionTitleProvider = {
//		$0.title
//	}
//
//})


class CalendarSelectionViewController: SelectionViewController<EKSource, EKCalendarCell>
{
	var sources: [EKSource]!





	override func viewDidLoad()
	{
		super.viewDidLoad()



		self.navigationItem.title = "Calendars"
		
		EKEventStore.default.sources
			.sorted(by: \EKSource.title.localizedLowercase, <)
			.forEach({ source in
				let calendars = source.calendars(for: .event)
					.sorted(by: \EKCalendar.title.localizedLowercase, <)
				
				self.dataProvider.append((source, calendars))
			})
		
//		self.dataProvider.sectionTitleProvider = {
//			$0.title
//		}
		
//		_ = viewController.selection
//			.takeUntil(viewController.rx.deallocated)
//			.bind(onNext: { [unowned viewController] calendar in
//				viewController.show(eventSelectionViewController(calendar), sender: nil)
//			})
//
//		return viewController


		
		
//
//		EKEventStore.default.sources
//			.sorted(by: \EKSource.title.localizedLowercase, <)
//			.forEach({ source in
//				let calendars = source.calendars(for: .event)
//					.sorted(by: \EKCalendar.title.localizedLowercase, <)
//
//				self.dataProvider.append(section: source, rows: calendars)
//			})
//
//		self.dataProvider.sectionTitleProvider = {
//			$0.title
//		}
	}

//	override func viewWillAppear(_ animated: Bool)
//	{
//		super.viewWillAppear(animated)
//	}
//
//	override func viewDidAppear(_ animated: Bool)
//	{
//		super.viewDidAppear(animated)
//	}
//
//	override func viewWillDisappear(_ animated: Bool)
//	{
//		super.viewWillDisappear(animated)
//	}
//
//	override func viewDidDisappear(_ animated: Bool)
//	{
//		super.viewDidDisappear(animated)
//	}
//
//
//
//	override func tableView(_ tableView: UITableView,
//							titleForHeaderInSection section: Int) -> String?
//	{
//		let section = self.dataProvider.section(at: section)
//
//		return section.value.title
//	}
}




