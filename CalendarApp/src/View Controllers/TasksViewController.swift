//
//  TasksViewController.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2018-10-27.
//  Copyright © 2018 Pat Sluth. All rights reserved.
//

import UIKit
import EventKit
import NotificationCenter

import Firebase
import FirebaseAuth
import FirebaseFirestore
import RxSwift
import RxCocoa
import RxSwiftExt
import SwiftDate
import Permission
import PromiseKit





class TasksViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
	@IBOutlet private var collectionView: UICollectionView!
	
	func numberOfSections(in collectionView: UICollectionView) -> Int
	{
		return self.tasks.count
	}
	
	func collectionView(_ collectionView: UICollectionView,
						numberOfItemsInSection section: Int) -> Int
	{
		let task = self.tasks[section]

		return task.ordinal
//		return self.tasks[section].value.reduce(into: 0, {
//			$0 += $1.ordinal
//		})
	}
	
	func collectionView(_ collectionView: UICollectionView,
						viewForSupplementaryElementOfKind kind: String,
						at indexPath: IndexPath) -> UICollectionReusableView
	{
		let view = collectionView.dequeueReusableSupplementaryView(of: TaskCellSectionHeader.self,
																   kind: kind,
																   for: indexPath)
		let task = self.tasks[indexPath.section]
		
		let date = Date()
		let end = date.dateAtEndOf(task.period.calendarComponent)
		let df = DateComponentsFormatter.init()
		df.unitsStyle = .full
		//df.collapsesLargestUnit = true
		//df.maximumUnitCount = 2
//		view.label.text = task.0.rawValue + " " + df.string(from: end.timeIntervalSince(date))!
		view.label.text = task.title + " " + df.string(from: end.timeIntervalSince(date))!
		
		return view
	}
	
//	func collectionView(_ collectionView: UICollectionView,
//						layout collectionViewLayout: UICollectionViewLayout,
//						sizeForItemAt indexPath: IndexPath) -> CGSize
//	{
//		return UICollectionViewFlowLayout.automaticSize
//	}
	
	func collectionView(_ collectionView: UICollectionView,
						layout collectionViewLayout: UICollectionViewLayout,
						referenceSizeForHeaderInSection section: Int) -> CGSize
	{
		return CGSize(width: collectionView.bounds.width, height: 50.0)
	}
	
	func collectionView(_ collectionView: UICollectionView,
						cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{
		let cell = collectionView.dequeueReusableCell(of: TaskCell.self, for: indexPath)
		let task = self.tasks[indexPath.section]
		
		cell.titleLabel.text = task.title
		cell.backgroundColor = task.color
		
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView,
						didSelectItemAt indexPath: IndexPath)
	{
		var task = self.tasks[indexPath.section]
//		tasks.1.remove(at: indexPath.item)
		task.ordinal -= 1
		self.tasks[indexPath.section] = task
//		self.tasks[indexPath.section].1 = tasks.1
		
//		collectionView.reloadSections(IndexSet(integer: indexPath.section))
		collectionView.deleteItems(at: [indexPath])
//		collectionView.layout
//		let task = self.tasks[indexPath.section]
	}
	
	
//	lazy var collectionView: UICollectionView = {
//		let tableView = UICollectionView()
//		tableView.translatesAutoresizingMaskIntoConstraints = false
//		tableView.delegate = self
//		return tableView
//	}()
	
//	var tasks: [Dictionary<Task.Period, [Task]>.Element]!
//	var tasks: [(Task.Period, [Task])]!
	var tasks: [Task]!
	
	
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		self.tasks = [
			Task(title: "A", period: Task.Period.Daily, ordinal: 5),
			Task(title: "B", period: Task.Period.Daily, ordinal: 14),
			Task(title: "C", period: Task.Period.Weekly, ordinal: 1),
			Task(title: "D", period: Task.Period.Weekly, ordinal: 6),
			Task(title: "E", period: Task.Period.Weekly, ordinal: 9)
			]
//			.grouped(by: \Task.period)
//			.mapValues({
////				$0.map({
////					Array(repeating: $0, count: $0.ordinal)
////				}).joined()
//				$0.reduce(into: [Task](), {
//					$0 += Array(repeating: $1, count: $1.ordinal)
//				})
//			})
//			.compactMap({
//				$0
//			})
		
		self.collectionView.registerCell(UINib(resource: R.nib.taskCell), of: TaskCell.self)
		self.collectionView.registerSupplementaryView(UINib(resource: R.nib.taskCellSectionHeader),
													  of: TaskCellSectionHeader.self,
													  kind: UICollectionView.elementKindSectionHeader)
	}
	
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
	}
	
	override func viewDidAppear(_ animated: Bool)
	{
		super.viewDidAppear(animated)
	}
	
	override func viewWillDisappear(_ animated: Bool)
	{
		super.viewWillDisappear(animated)
	}
	
	override func viewDidDisappear(_ animated: Bool)
	{
		super.viewDidDisappear(animated)
	}
}




