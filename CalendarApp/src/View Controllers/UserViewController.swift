//
//  UserViewController.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2018-10-07.
//  Copyright © 2018 Pat Sluth. All rights reserved.
//

import UIKit

//import Firebase
//import FirebaseAuth
//import FirebaseFirestore
import RxSwift
import RxCocoa
import RxSwiftExt
//import RxFirebaseAuth
//import RxFirebase

//import Eureka
import Alertift




class UserViewController: UIViewController
{
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
//		_ = Auth.auth().rx.currentUser()
//			.takeUntil(self.rx.deallocated)
//			.subscribe(onNext: { user in
//				if let user = user {
//					self.showUserAlert(user)
//				} else {
//					self.showLoginRegisterAlert()
//				}
//			})
	}
	
	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
	}
	
	override func viewDidAppear(_ animated: Bool)
	{
		super.viewDidAppear(animated)
	}
	
	@IBAction private func doneButtonClicked(_ sender: Any)
	{
		self.dismiss(animated: true, completion: nil)
	}
}





//fileprivate extension UserViewController
//{
//	func showUserAlert(_ user: User)
//	{
//		Alertift.alert(title: "Current User", message: "\(user.uid)\(Char.NewLine)\(user.email ?? "")")
//			.action(.cancel("Cancel"))
//			.action(.default("Logout")) { _, _, _ in
//				try? Auth.auth().signOut()
//			}
//			.show(on: self, completion: nil)
//	}
//
//	func showLoginRegisterAlert()
//	{
//		Alertift.alert(title: nil, message: nil)
//			.action(.cancel("Cancel"))
//			.action(.default("Login")) { _, _, _ in
//				self.showLoginAlert()
//			}
//			.action(.default("Register")) { _, _, _ in
//				self.showRegisterAlert()
//			}
//			.show(on: self, completion: nil)
//	}
//
//	func showLoginAlert()
//	{
//		Alertift.alert(title: "Login", message: nil)
//			.textField { textField in
//				textField.placeholder = "Email"
//				textField.keyboardType = UIKeyboardType.emailAddress
//				textField.text = "pat.sluth@gmail.com"
//			}
//			.textField { textField in
//				textField.placeholder = "Password"
//				textField.isSecureTextEntry = true
//				textField.text = "abcd1234"
//			}
//			.action(.cancel("Cancel"))
//			.action(.default("Login")) { _, _, textFields in
//				guard let email = textFields?[safe: 0]?.text else { return }
//				guard let password = textFields?[safe: 1]?.text else { return }
//
//				Auth.auth().rx.login(email, password)
//			}
//			.show(on: self, completion: nil)
//	}
//
//	func showRegisterAlert()
//	{
//		Alertift.alert(title: "Register", message: nil)
//			.textField { textField in
//				textField.placeholder = "First Name"
//				textField.keyboardType = UIKeyboardType.default
//				textField.text = "Pat"
//			}
//			.textField { textField in
//				textField.placeholder = "Last Name"
//				textField.keyboardType = UIKeyboardType.default
//				textField.text = "Sluth"
//			}
//			.textField { textField in
//				textField.placeholder = "Email"
//				textField.keyboardType = UIKeyboardType.emailAddress
//				textField.text = "pat.sluth@gmail.com"
//			}
//			.textField { textField in
//				textField.placeholder = "Password"
//				textField.isSecureTextEntry = true
//				textField.text = "abcd1234"
//			}
//			.textField { textField in
//				textField.placeholder = "Confirm Password"
//				textField.isSecureTextEntry = true
//				textField.text = "abcd1234"
//			}
//			.action(.cancel("Cancel"))
//			.action(.default("Register")) { _, _, textFields in
//
//				// TODO: check passwords match
//
//				guard let firstName = textFields?[safe: 0]?.text else { return }
//				guard let lastName = textFields?[safe: 1]?.text else { return }
//				guard let email = textFields?[safe: 2]?.text else { return }
//				guard let password = textFields?[safe: 3]?.text else { return }
//				guard let confirmPassword = textFields?[safe: 4]?.text else { return }
//
//				let profile = Profile(firstName: firstName,
//									  lastName: lastName)
//
//				Auth.auth().rx.register(email, password)
//					.done({ authDataResult in
//						authDataResult.user.document.fieldOf(Profile.self)
//							.set(value: profile)
//							.done({ _ in
//							})
//							.catch({ error in
//								// Ensure profile is set
//								authDataResult.user.delete()
//							})
//					})
//					.catch({ error in
//						print(#file.fileName, #function, error.localizedDescription)
//					})
//			}
//			.show(on: self, completion: nil)
//	}
//}




