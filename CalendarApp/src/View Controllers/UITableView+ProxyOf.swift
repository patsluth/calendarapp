//
//  UITableView+ProxyOf.swift
//  Sluthware
//
//  Created by Pat Sluth on 2018-10-27.
//  Copyright © 2018 Pat Sluth. All rights reserved.
//

import UIKit

import RxSwift
import RxCocoa





public extension UITableView
{
	public enum Proxy
	{
		public typealias DataSource = _UITableViewProxyDataSource
		public typealias Delegate = _UITableViewProxyDelegate
//		public typealias Both = _UITableViewProxyBoth
	}
}

//public extension UITableView
//{
//	public typealias SingleSelectionProxy = _UITableViewDelegateProxySingleSelection
//	public typealias MultipleSelectionProxy = _UITableViewDelegateProxyMultipleSelection
//}




//open class _UITableViewProxyBase<T>: ProxyOf<T>
//	where T: AnyObject
//{
//	open weak var tableView: UITableView! {
//		didSet
//		{
//			self.base = self.tableView.dataSource
//			self.tableView.dataSource = self
//
//			self.tableView.reloadData()
//		}
//	}
//}







public extension UITableView
{
	var proxyDataSource: UITableView.Proxy.DataSource? {
		get
		{
			return self.get(associatedObject: "proxyDataSource",
							UITableView.Proxy.DataSource.self)
		}
		set
		{
			self.dataSource = self.proxyDataSource?.base ?? self.dataSource
			self.set(associatedObject: "proxyDataSource", object: newValue)
			newValue?.tableView = self
			
			self.reloadData()
		}
	}
	
	var proxyDelegate: UITableView.Proxy.Delegate? {
		get
		{
			return self.get(associatedObject: "proxyDelegate",
							UITableView.Proxy.Delegate.self)
		}
		set
		{
			self.delegate = self.proxyDelegate?.base ?? self.delegate
			self.set(associatedObject: "proxyDelegate", object: newValue)
			newValue?.tableView = self
			
			self.reloadData()
		}
	}
}







open class _UITableViewProxyDataSource: ProxyOf<UITableViewDataSource>, UITableViewDataSource
{
	open weak var tableView: UITableView! {
		didSet
		{
			self.base = self.tableView.dataSource
			self.tableView.dataSource = self
		}
	}





	// MARK: UITableViewDataSource

	@available(iOS 2.0, *)
	open func tableView(_ tableView: UITableView,
						numberOfRowsInSection section: Int) -> Int
	{
		fatalError("\(#function) not implemented")
	}

	@available(iOS 2.0, *)
	open func tableView(_ tableView: UITableView,
						cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		fatalError("\(#function) not implemented")
	}

	@available(iOS 2.0, *)
	open func numberOfSections(in tableView: UITableView) -> Int
	{
		return self.base?.numberOfSections?(in: tableView) ?? 0
	}

	@available(iOS 2.0, *)
	open func tableView(_ tableView: UITableView,
						titleForHeaderInSection section: Int) -> String?
	{
		return self.base?.tableView?(tableView, titleForHeaderInSection: section)
	}

	@available(iOS 2.0, *)
	open func tableView(_ tableView: UITableView,
						titleForFooterInSection section: Int) -> String?
	{
		return self.base?.tableView?(tableView, titleForFooterInSection: section)
	}

	@available(iOS 2.0, *)
	open func tableView(_ tableView: UITableView,
						canEditRowAt indexPath: IndexPath) -> Bool
	{
		return self.base?.tableView?(tableView, canEditRowAt: indexPath) ?? true
	}

	@available(iOS 2.0, *)
	open func tableView(_ tableView: UITableView,
						canMoveRowAt indexPath: IndexPath) -> Bool
	{
		return self.base?.tableView?(tableView, canMoveRowAt: indexPath) ?? true
	}

	@available(iOS 2.0, *)
	open func sectionIndexTitles(for tableView: UITableView) -> [String]?
	{
		return self.base?.sectionIndexTitles?(for: tableView)
	}

	//	@available(iOS 2.0, *)
	//	open func tableView(_ tableView: UITableView,
	//						sectionForSectionIndexTitle title: String,
	//						at index: Int) -> Int
	//	{
	//		return self.base?.tableView?(tableView, sectionForSectionIndexTitle: title, at: index)
	//	}

	@available(iOS 2.0, *)
	open func tableView(_ tableView: UITableView,
						commit editingStyle: UITableViewCell.EditingStyle,
						forRowAt indexPath: IndexPath)
	{
		self.base?.tableView?(tableView, commit: editingStyle, forRowAt: indexPath)
	}

	@available(iOS 2.0, *)
	open func tableView(_ tableView: UITableView,
						moveRowAt sourceIndexPath: IndexPath,
						to destinationIndexPath: IndexPath)
	{
		self.base?.tableView?(tableView, moveRowAt: sourceIndexPath, to: destinationIndexPath)
	}
}





//extension _UITableViewDelegateProxy: BlockInitializable
//{
//
//}

//extension _UITableViewProxyDelegate: BlockInitializable
//{
//
//}


//open class _UITableViewProxyBoth: ProxyOf<UITableViewDataSource & UITableViewDelegate>, UITableViewDataSource, UITableViewDelegate
//{
//	open weak var tableView: UITableView! {
//		didSet
//		{
//			self.base = self.tableView.delegate & self.tableView.dataSource
//			self.tableView.delegate = self
//		}
//	}
//
//	public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
//	{
//		fatalError("\(#function) not implemented")
//	}
//
//	public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
//	{
//		fatalError("\(#function) not implemented")
//	}
//}

open class _UITableViewProxyDelegate: ProxyOf<UITableViewDelegate>, UITableViewDelegate
{
	open weak var tableView: UITableView! {
		didSet
		{
			self.base = self.tableView.delegate
			self.tableView.delegate = self
		}
	}
	
	
	
	
	
//	convenience public init(_ tableView: UITableView)
//	{
//		self.init()
//
//		defer {
//			self.tableView = tableView
//		}
//	}
	
//	deinit
//	{
//
//	}
	
	
	
//	convenience init(_ block: (_UITableViewDelegateProxy) -> Void)
//	{
//		self.init()
//
//		defer {
//			block(self)
//		}
//	}
	
//	required public init(_ base: UITableViewDelegate?)
//	{
//		super.init(tableView.delegate)
//	}
//
//	required public init(_ tableView: UITableView)
//	{
//		super.init(tableView.delegate)
//
//		defer {
//			self.tableView = tableView
//		}
//	}
	//
//	public required init(_ block: (_UITableViewDelegateProxy) -> Void)
//	{
//		block(self)
//	}
	
//	required public init(_ tableView: UITableView?)
//	{
//		super.init(tableView?.delegate)
//	}
//
//	convenience init(_ block: (_UITableViewDelegateProxy) -> Void)
//	{
//		self.init(nil)
//
//		defer {
//			block(self)
//		}
//	}
	
//	public required init(_ tableView: UITableView)
//	{
//		super.init(tableView.delegate)
//	}
//
//	convenience init(_ block: (_UITableViewDelegateProxy) -> Void)
//	{
//		self.init(nil)
//
//		block(self)
//	}
	
//	required public init(_ tableView: UITableView)
//	{
//		super.init(tableView.delegate)
//
//		tableView.delegate = nil
//		tableView.rx.delegate.setForwardToDelegate(self, retainDelegate: false)
//	}
//
//	required public init()
//	{
//		super.init()
//	}
	
	
	
	
	
	// MARK: UITableViewDelegate
	
	@available(iOS 2.0, *)
	open func tableView(_ tableView: UITableView,
						willDisplay cell: UITableViewCell,
						forRowAt indexPath: IndexPath)
	{
		self.base?.tableView?(tableView,
							  willDisplay: cell,
							  forRowAt: indexPath)
	}
	
	@available(iOS 6.0, *)
	open func tableView(_ tableView: UITableView,
						willDisplayHeaderView view: UIView,
						forSection section: Int)
	{
		self.base?.tableView?(tableView,
							  willDisplayHeaderView: view,
							  forSection: section)
	}
	
	@available(iOS 6.0, *)
	open func tableView(_ tableView: UITableView,
						willDisplayFooterView view: UIView,
						forSection section: Int)
	{
		self.base?.tableView?(tableView,
							  willDisplayFooterView: view,
							  forSection: section)
	}
	
	@available(iOS 6.0, *)
	open func tableView(_ tableView: UITableView,
						didEndDisplaying cell: UITableViewCell,
						forRowAt indexPath: IndexPath)
	{
		self.base?.tableView?(tableView,
							  didEndDisplaying: cell,
							  forRowAt: indexPath)
	}
	
	@available(iOS 6.0, *)
	open func tableView(_ tableView: UITableView,
						didEndDisplayingHeaderView view: UIView,
						forSection section: Int)
	{
		self.base?.tableView?(tableView,
							  didEndDisplayingHeaderView: view,
							  forSection: section)
	}
	
	@available(iOS 6.0, *)
	open func tableView(_ tableView: UITableView,
						didEndDisplayingFooterView view: UIView,
						forSection section: Int)
	{
		self.base?.tableView?(tableView,
							  didEndDisplayingFooterView: view,
							  forSection: section)
	}
	
//	@available(iOS 2.0, *)
//	open func tableView(_ tableView: UITableView,
//						heightForRowAt indexPath: IndexPath) -> CGFloat
//	{
//		return self.base?.tableView?(tableView,
//									 heightForRowAt: indexPath) ?? tableView.rowHeight
//	}
//
//	@available(iOS 2.0, *)
//	open func tableView(_ tableView: UITableView,
//						heightForHeaderInSection section: Int) -> CGFloat
//	{
//		return self.base?.tableView?(tableView,
//									 heightForHeaderInSection: section) ?? tableView.sectionHeaderHeight
//	}
//
//	@available(iOS 2.0, *)
//	open func tableView(_ tableView: UITableView,
//						heightForFooterInSection section: Int) -> CGFloat
//	{
//		return self.base?.tableView?(tableView,
//									 heightForFooterInSection: section) ?? tableView.sectionFooterHeight
//	}
//
//	@available(iOS 7.0, *)
//	open func tableView(_ tableView: UITableView,
//						estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
//	{
//		return self.base?.tableView?(tableView,
//									 estimatedHeightForRowAt: indexPath) ?? tableView.estimatedRowHeight
//	}
//
//	@available(iOS 7.0, *)
//	open func tableView(_ tableView: UITableView,
//						estimatedHeightForHeaderInSection section: Int) -> CGFloat
//	{
//		return self.base?.tableView?(tableView,
//									 estimatedHeightForHeaderInSection: section) ?? tableView.estimatedSectionHeaderHeight
//	}
//
//	@available(iOS 7.0, *)
//	open func tableView(_ tableView: UITableView,
//						estimatedHeightForFooterInSection section: Int) -> CGFloat
//	{
//		return self.base?.tableView?(tableView,
//									 estimatedHeightForFooterInSection: section) ?? tableView.estimatedSectionFooterHeight
//	}
//
//	@available(iOS 2.0, *)
//	open func tableView(_ tableView: UITableView,
//						viewForHeaderInSection section: Int) -> UIView?
//	{
//		return self.base?.tableView?(tableView,
//									 viewForHeaderInSection: section)
//	}
//
//	@available(iOS 2.0, *)
//	open func tableView(_ tableView: UITableView,
//						viewForFooterInSection section: Int) -> UIView?
//	{
//		return self.base?.tableView?(tableView,
//									 viewForFooterInSection: section)
//	}
	
	@available(iOS 2.0, *)
	open func tableView(_ tableView: UITableView,
						accessoryButtonTappedForRowWith indexPath: IndexPath)
	{
		self.base?.tableView?(tableView,
							  accessoryButtonTappedForRowWith: indexPath)
	}
	
	@available(iOS 6.0, *)
	open func tableView(_ tableView: UITableView,
						shouldHighlightRowAt indexPath: IndexPath) -> Bool
	{
		return self.base?.tableView?(tableView,
									 shouldHighlightRowAt: indexPath) ?? true
	}
	
	@available(iOS 6.0, *)
	open func tableView(_ tableView: UITableView,
						didHighlightRowAt indexPath: IndexPath)
	{
		self.base?.tableView?(tableView,
							  didHighlightRowAt: indexPath)
	}
	
	@available(iOS 6.0, *)
	open func tableView(_ tableView: UITableView,
						didUnhighlightRowAt indexPath: IndexPath)
	{
		self.base?.tableView?(tableView,
							  didUnhighlightRowAt: indexPath)
	}
	
	@available(iOS 2.0, *)
	open func tableView(_ tableView: UITableView,
						willSelectRowAt indexPath: IndexPath) -> IndexPath?
	{
		if tableView.allowsSelection {
			return indexPath
		} else {
			return self.base?.tableView?(tableView,
										 willSelectRowAt: indexPath)
		}
	}
	
	@available(iOS 3.0, *)
	open func tableView(_ tableView: UITableView,
						willDeselectRowAt indexPath: IndexPath) -> IndexPath?
	{
		if tableView.allowsSelection {
			return indexPath
		} else {
			return self.base?.tableView?(tableView,
										 willDeselectRowAt: indexPath)
		}
	}
	
	@available(iOS 2.0, *)
	open func tableView(_ tableView: UITableView,
						didSelectRowAt indexPath: IndexPath)
	{
		self.base?.tableView?(tableView,
							  didSelectRowAt: indexPath)
	}
	
	@available(iOS 3.0, *)
	open func tableView(_ tableView: UITableView,
						didDeselectRowAt indexPath: IndexPath)
	{
		self.base?.tableView?(tableView,
							  didDeselectRowAt: indexPath)
	}
	
	@available(iOS 2.0, *)
	open func tableView(_ tableView: UITableView,
						editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle
	{
		return self.base?.tableView?(tableView,
									 editingStyleForRowAt: indexPath) ?? .none
	}
	
	@available(iOS 3.0, *)
	open func tableView(_ tableView: UITableView,
						titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String?
	{
		return self.base?.tableView?(tableView,
									 titleForDeleteConfirmationButtonForRowAt: indexPath)
	}
	
	@available(iOS 8.0, *)
	open func tableView(_ tableView: UITableView,
						editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
	{
		return self.base?.tableView?(tableView,
									 editActionsForRowAt: indexPath)
	}
	
	@available(iOS 11.0, *)
	open func tableView(_ tableView: UITableView,
						leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
	{
		return self.base?.tableView?(tableView,
									 leadingSwipeActionsConfigurationForRowAt: indexPath)
	}
	
	@available(iOS 11.0, *)
	open func tableView(_ tableView: UITableView,
						trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
	{
		return self.base?.tableView?(tableView,
									 trailingSwipeActionsConfigurationForRowAt: indexPath)
	}
	
	@available(iOS 2.0, *)
	open func tableView(_ tableView: UITableView,
						shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool
	{
		return self.base?.tableView?(tableView,
									 shouldIndentWhileEditingRowAt: indexPath) ?? true
	}
	
	@available(iOS 2.0, *)
	open func tableView(_ tableView: UITableView,
						willBeginEditingRowAt indexPath: IndexPath)
	{
		self.base?.tableView?(tableView,
							  willBeginEditingRowAt: indexPath)
	}
	
	@available(iOS 2.0, *)
	open func tableView(_ tableView: UITableView,
						didEndEditingRowAt indexPath: IndexPath?)
	{
		self.base?.tableView?(tableView,
							  didEndEditingRowAt: indexPath)
	}
	
	@available(iOS 2.0, *)
	open func tableView(_ tableView: UITableView,
						targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath,
						toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath
	{
		let indexPath = self.base?.tableView?(tableView,
											  targetIndexPathForMoveFromRowAt: sourceIndexPath,
											  toProposedIndexPath: proposedDestinationIndexPath)
		return indexPath ?? sourceIndexPath
	}
	
	@available(iOS 2.0, *)
	open func tableView(_ tableView: UITableView,
						indentationLevelForRowAt indexPath: IndexPath) -> Int
	{
		return self.base?.tableView?(tableView,
									 indentationLevelForRowAt: indexPath) ?? 0
	}
	
	@available(iOS 5.0, *)
	open func tableView(_ tableView: UITableView,
						shouldShowMenuForRowAt indexPath: IndexPath) -> Bool
	{
		return self.base?.tableView?(tableView,
									 shouldShowMenuForRowAt: indexPath) ?? false
	}
	
	@available(iOS 5.0, *)
	open func tableView(_ tableView: UITableView,
						canPerformAction action: Selector,
						forRowAt indexPath: IndexPath,
						withSender sender: Any?) -> Bool
	{
		return self.base?.tableView?(tableView,
									 canPerformAction: action,
									 forRowAt: indexPath,
									 withSender: sender) ?? false
	}
	
	@available(iOS 5.0, *)
	open func tableView(_ tableView: UITableView,
						performAction action: Selector,
						forRowAt indexPath: IndexPath,
						withSender sender: Any?)
	{
		self.base?.tableView?(tableView,
							  performAction: action,
							  forRowAt: indexPath,
							  withSender: sender)
	}
	
	@available(iOS 9.0, *)
	open func tableView(_ tableView: UITableView,
						canFocusRowAt indexPath: IndexPath) -> Bool
	{
		return self.base?.tableView?(tableView,
									 canFocusRowAt: indexPath) ?? false
	}
	
	@available(iOS 9.0, *)
	open func tableView(_ tableView: UITableView,
						shouldUpdateFocusIn context: UITableViewFocusUpdateContext) -> Bool
	{
		return self.base?.tableView?(tableView,
									 shouldUpdateFocusIn: context) ?? false
	}
	
	@available(iOS 9.0, *)
	open func tableView(_ tableView: UITableView,
						didUpdateFocusIn context: UITableViewFocusUpdateContext,
						with coordinator: UIFocusAnimationCoordinator)
	{
		self.base?.tableView?(tableView,
							  didUpdateFocusIn: context,
							  with: coordinator)
	}
	
	@available(iOS 9.0, *)
	open func indexPathForPreferredFocusedView(in tableView: UITableView) -> IndexPath?
	{
		return self.base?.indexPathForPreferredFocusedView?(in: tableView)
	}
	
	@available(iOS 11.0, *)
	open func tableView(_ tableView: UITableView,
						shouldSpringLoadRowAt indexPath: IndexPath,
						with context: UISpringLoadedInteractionContext) -> Bool
	{
		return self.base?.tableView?(tableView,
									 shouldSpringLoadRowAt: indexPath,
									 with: context) ?? false
	}
}




//public class ModelSelectionProxy<Cell>: SingleSelectionProxy
//	where Cell: UITableView.ModelCell
//	//	where Cell: ModelViewProtocol, Cell: UITableView.ModelCell<>
//{
//	public var didSelect: ((IndexPath) -> Void)? = nil
//
//	public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
//	{
//		super.tableView?(tableView, didSelectRowAt: indexPath)
//	}
//}





//public class _UITableViewDelegateProxySingleSelection: UITableView.Proxy.Delegate
public class SingleSelectionProxy: UITableView.Proxy.Delegate
{
	public override var tableView: UITableView! {
		didSet
		{
			self.tableView.allowsSelection = true
			self.tableView.allowsMultipleSelection = false
		}
	}
	
//	public func bind<Section, Cell, T>(to dataSource: T) -> Observable<Cell.Model>
//		where Cell: UITableView.ModelCell, T: ModelDataSourceProxy<Section, Cell>
//	{
//		return self.selection
//			.unwrap()
//			.map({ [unowned dataSource] indexPath in
//				dataSource.sections[indexPath.section].elements[indexPath.row]
//			})
//
////		return Observable.create({ Observable in
////
////		})
//	}
	
//	public class ModelDataSourceProxy<Model, Cell>: UITableView.Proxy.DataSource
//		where Cell: UITableView.ModelCell
//		//	where Cell: ModelViewProtocol, Cell: UITableView.ModelCell<>
//	{
//		public typealias Section = ModelSection<Model, Cell>
	
	let selection = BehaviorSubject<IndexPath?>(value: nil)
	
//	public required init(test: BehaviorSubject<IndexPath>)
//	{
//
//	}
	
	
	
	
	
	public override func tableView(_ tableView: UITableView,
								   willDisplay cell: UITableViewCell,
								   forRowAt indexPath: IndexPath)
	{
		super.tableView(tableView, willDisplay: cell, forRowAt: indexPath)
		
		if indexPath == tableView.indexPathForSelectedRow {
			cell.accessoryType = UITableViewCell.AccessoryType.checkmark
		} else {
			cell.accessoryType = UITableViewCell.AccessoryType.none
		}
	}
	
	public override func tableView(_ tableView: UITableView,
								   didEndDisplaying cell: UITableViewCell,
								   forRowAt indexPath: IndexPath)
	{
		super.tableView(tableView, didEndDisplaying: cell, forRowAt: indexPath)
		
		cell.accessoryType = UITableViewCell.AccessoryType.none
	}
	
	public override func tableView(_ tableView: UITableView,
								   willSelectRowAt indexPath: IndexPath) -> IndexPath?
	{
		if indexPath == tableView.indexPathForSelectedRow {
			tableView.deselectRow(at: indexPath, animated: true)
			tableView.delegate?.tableView?(tableView, didDeselectRowAt: indexPath)
			return nil
		} else {
			return indexPath
		}
	}
	
	public override func tableView(_ tableView: UITableView,
								   didDeselectRowAt indexPath: IndexPath)
	{
		super.tableView(tableView, didDeselectRowAt: indexPath)
		
		let cell = tableView.cellForRow(at: indexPath)
		cell?.accessoryType = UITableViewCell.AccessoryType.none
		
		self.selection.onNext(tableView.indexPathForSelectedRow)
	}
	
	public override func tableView(_ tableView: UITableView,
								   didSelectRowAt indexPath: IndexPath)
	{
		super.tableView(tableView, didSelectRowAt: indexPath)
		
		let cell = tableView.cellForRow(at: indexPath)
		cell?.accessoryType = UITableViewCell.AccessoryType.checkmark
		
		self.selection.onNext(tableView.indexPathForSelectedRow)
	}
}





//public class _UITableViewDelegateProxyMultipleSelection: UITableView.Proxy.Delegate
public class MultipleSelectionProxy: UITableView.Proxy.Delegate
{
	//	var selectionClosure: (([IndexPath]?) -> Void)? = nil
	public override var tableView: UITableView! {
		didSet
		{
			self.tableView.allowsSelection = true
			self.tableView.allowsMultipleSelection = true
		}
	}
	
	let selection = BehaviorSubject<[IndexPath]?>(value: nil)
	
	
	
	
	
	public override func tableView(_ tableView: UITableView,
								   willDisplay cell: UITableViewCell,
								   forRowAt indexPath: IndexPath)
	{
		super.tableView(tableView, willDisplay: cell, forRowAt: indexPath)
		
		if tableView.indexPathsForSelectedRows?.contains(indexPath) ?? false {
			cell.accessoryType = UITableViewCell.AccessoryType.checkmark
		} else {
			cell.accessoryType = UITableViewCell.AccessoryType.none
		}
	}
	
	public override func tableView(_ tableView: UITableView,
								   didEndDisplaying cell: UITableViewCell,
								   forRowAt indexPath: IndexPath)
	{
		super.tableView(tableView, didEndDisplaying: cell, forRowAt: indexPath)
		
		cell.accessoryType = UITableViewCell.AccessoryType.none
	}
	
	public override func tableView(_ tableView: UITableView,
								   didDeselectRowAt indexPath: IndexPath)
	{
		super.tableView(tableView, didDeselectRowAt: indexPath)
		
		let cell = tableView.cellForRow(at: indexPath)
		cell?.accessoryType = UITableViewCell.AccessoryType.none
		print(tableView.indexPathsForSelectedRows)
		self.selection.onNext(tableView.indexPathsForSelectedRows)
	}
	
	public override func tableView(_ tableView: UITableView,
								   didSelectRowAt indexPath: IndexPath)
	{
		super.tableView(tableView, didSelectRowAt: indexPath)
		
		let cell = tableView.cellForRow(at: indexPath)
		cell?.accessoryType = UITableViewCell.AccessoryType.checkmark
		
		print(tableView.indexPathsForSelectedRows)
		self.selection.onNext(tableView.indexPathsForSelectedRows)
	}
}





class DataSourceRow
{
	
}

public protocol ModelSectionProtocol: class
{
	associatedtype Model
	associatedtype Cell: ModelView
	
	typealias Element = Cell.Model
	typealias Elements = [Element]
	
	
	
	
	
	var model: Model { get }
	var elements: Elements { get }
	
	init(_ model: Model, _ elements: Elements)
}

//public protocol SelectableModelSectionProtocol: ModelSectionProtocol
//{
//	var isSelected: Bool { get set }
//}

public extension ModelSectionProtocol
{
	public init(_ model: Model)
	{
		self.init(model, [])
	}
	
	public init(_ model: Model, _ block: () -> Elements)
	{
		self.init(model, block())
	}
}










public extension UITableView
{
//	public typealias ModelSection = _UITableViewModelSection
}

open class ModelSection<_Model, _Cell>: ModelSectionProtocol
	where _Cell: UITableView.ModelCell
{
	public typealias Model = _Model
	public typealias Cell = _Cell
	
	public fileprivate(set) var model: Model
	public fileprivate(set) var elements: Elements
	
	required public init(_ model: Model, _ elements: Elements)
	{
		self.model = model
		self.elements = elements
	}
//
//	func element(at index: Int) -> Element
//	{
//		return self.sections[index.section].1[index.row]
//	}
}

//open class SelectableModelSection<_Model, _Cell>: ModelSection<_Model, _Cell>, SelectableModelSectionProtocol
//	where _Cell: SelectableModelViewProtocol
//{
//	public var isSelected: Bool = false
//}



//public protocol ModelSelectionProtocol
//{
//	associatedtype SelectionType
//}
//
//public class SingleSelection: SelectionProtocol
//{
//
//}


public class ModelDataSourceProxy<Model, Cell>: UITableView.Proxy.DataSource, UITableViewDelegate
	where Cell: UITableView.ModelCell
//	where Cell: ModelViewProtocol, Cell: UITableView.ModelCell<>
{
	public typealias Section = ModelSection<Model, Cell>
	public typealias Sections = [Section]
	
	public typealias TitleProvider = (Model) -> String?
	
	public typealias SingleSelection = (Section.Element) -> Void
	public typealias MultipleSelection = ([Section.Element]) -> Void
	
	public enum Selection
	{
		case Single((Section.Element) -> Void)
		case Multiple(([Section.Element]) -> Void)
	}
	
	
//	typealias CellType = Cell
//	typealias IndexType = IndexPath
//	typealias Sections = [ModelSection<Section, Cell>]
//	typealias Rows = [Cell.Model]
	
//	typealias SectionTitleProvider = (Section) -> String?
	//	typealias CellSelected = (Cell, Cell.Value) -> Void
	
	
	
	public fileprivate(set) var sections = Sections()
	
	public override var tableView: UITableView! {
		didSet
		{
			self.tableView.register(Cell.nib, of: Cell.self)
			self.tableView.delegate = self
			
			self.selection = { self.selection }()
		}
	}
	
	public var headerTitleProvider: TitleProvider? = nil {
		didSet { self.tableView?.reloadData() }
	}
	
	public var footerTitleProvider: TitleProvider? = nil {
		didSet { self.tableView?.reloadData() }
	}
	
	public var selection: Selection? = nil {
		didSet
		{
			switch self.selection {
			case .Single(_)?:
				self.tableView?.allowsSelection = true
				self.tableView?.allowsMultipleSelection = false
			case .Multiple(_)?:
				self.tableView?.allowsSelection = true
				self.tableView?.allowsMultipleSelection = true
			default:
				self.tableView?.allowsSelection = false
				self.tableView?.allowsMultipleSelection = false
			}
		}
	}
	
//	public required init(selecton: Selection)
//	{
//		self.selection = selection
//
//		super.init()
//	}
	
//	public var singleSelection: SingleSelection? = nil {
//		didSet
//		{
//			if let _ = self.singleSelection {
//				self.multipleSelection = nil
//				self.tableView.allowsSelection = true
//				self.tableView.allowsMultipleSelection = false
//			}
//		}
//	}
//	public var multipleSelection: MultipleSelection? = nil {
//		didSet
//		{
//			if let _ = self.multipleSelection {
//				self.singleSelection = nil
//				self.tableView.allowsSelection = true
//				self.tableView.allowsMultipleSelection = true
//			}
//		}
//	}
	
//	public var selection: ExclusivePair<SingleSelection, MultipleSelection>? = nil {
//		didSet
//		{
//			self.tableView.allowsSelection = true
//
//			switch self.selection {
//			case .A(_)?:
//				self.tableView.allowsMultipleSelection = false
//			case .B(_)?:
//				self.tableView.allowsMultipleSelection = true
//			default:
//				break
//			}
//		}
//	}
	
	public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		switch self.selection {
		case .Single(let block)?:
			let model = self.sections[indexPath.section].elements[indexPath.row]
			block(model)
		case .Multiple(let block)?:
			let indexPaths = tableView.indexPathsForSelectedRows ?? []
			let models = indexPaths.map({ indexPath in
				self.sections[indexPath.section].elements[indexPath.row]
			})
			block(models)
		default:
			break
		}
		
//		if let selection = self.singleSelection {
//			selection(self.sections[indexPath.section].elements[indexPath.row])
//		}
//
//		if let selection = self.multipleSelection {
//			let indexPaths = tableView.indexPathsForSelectedRows ?? []
//			let models = indexPaths.map({ indexPath in
//				self.sections[indexPath.section].elements[indexPath.row]
//			})
//			selection(models)
//		}
		
		
//		switch self.selection {
//		case .A(let singleSelection)?:
//			singleSelection(model, cell)
//		case .B(let multipleSelection)?:
//		default:
//			break
//		}
//		self.selection?(model, cell)
	}
	
//	public var headerTitleKeyPath: PartialKeyPath<Model>? = nil {
//		didSet { self.tableView?.reloadData() }
//	}
//
//	public var footerTitleKeyPath: PartialKeyPath<Model>? = nil {
//		didSet { self.tableView?.reloadData() }
//	}
	
	
	
//	private let disposeBag = DisposeBag()
	//	var selection: BehaviorRelay<Cell.Value?>(value: nil)? = nil
	
//	var sectionTitleProvider: SectionTitleProvider! = nil
	//var cellSelected: CellSelected! = nil
	//	typealias SectionTitleProvider = (Section) -> String
	//	var sectionTitleKeyPath: SectionTitleProvider? = nil
	
	
	
	
	
	
//	required init(tableView: UITableView)
//	{
//		self.tableView = tableView
//		//		self.selection = selection
//
//		super.init()
//
//		//		defer {
//		self.tableView.register(Cell.nib, of: Cell.self)
//
//		self.tableView.rx.setDataSource(self)
//			.disposed(by: self.disposeBag)
//		//		self.tableView.rx.setDelegate(self)
//		//			.disposed(by: self.disposeBag)
//		//			self.tableView.dataSource = self
//
//		//			self.tableView.delegate = self
//		//		}
//	}
	
//	public func row(at index: IndexPath) -> Cell.Model
//	{
//		return self.sections[index.section].1[index.row]
//	}
	
	//	convenience init()
	//	{
	//		self.init()
	//
	//		initBlock(self)
	//	}
	
	
	
	
	func append(_ section: Sections.Element,
				with animation: UITableView.RowAnimation = .none)// -> Rows.Index
	{
		self.insert(section, at: self.sections.endIndex, with: animation)
		//		self.insert(row, at: self.sections.endIndex, with: animation)
		//		self.rows.append(row)
		//return self.rows.endIndex.advanced(by: -1)
	}
	
	//	func append(_ row: Rows.Element,
	//				with animation: UITableView.RowAnimation = .none)// -> Rows.Index
	//	{
	//		self.insert(row, at: self.rows.endIndex, with: animation)
	//		//		self.rows.append(row)
	//		//return self.rows.endIndex.advanced(by: -1)
	//	}
	
	//	@discardableResult
	//	func append(rows: Rows) -> [Rows.Index]
	//	{
	//		let section = Sections.Element(value: value, rows: rows)
	//		return self.append(section: section)
	//	}
	
	//	@discardableResult
	//	func append(section value: Section, rows: Row...) -> Sections.Index
	//	{
	//		return self.append(section: section, rows: rows)
	//	}
	
	func insert(_ section: Sections.Element,
				at index: Int,
				with animation: UITableView.RowAnimation = .none)
	{
		self.sections.insert(section, at: index)
		
		self.tableView.insertSections(IndexSet(integer: index), with: animation)
	}
	
	@discardableResult
	func remove(at index: Int,
				with animation: UITableView.RowAnimation = .none) -> Sections.Element
	{
		let section = self.sections.remove(at: index)
		
		self.tableView.deleteSections(IndexSet(integer: index), with: animation)
		
		return section
	}
	
	
	
	
	func insert(_ element: Section.Element,
				at indexPath: IndexPath,
				with animation: UITableView.RowAnimation = .none)
	{
		self.sections[indexPath.section].elements.insert(element, at: indexPath.row)
		
		self.tableView.insertRows(at: [indexPath], with: animation)
		//		self.tableView.insertRows(at: [indexPath], with: animation)
		//		return index
	}
	
	@discardableResult
	func remove(at indexPath: IndexPath,
				with animation: UITableView.RowAnimation = .none) -> Section.Element
	{
		let element = self.sections[indexPath.section].elements.remove(at: indexPath.row)
		
		self.tableView.deleteRows(at: [indexPath], with: animation)
		
		return element
	}
	
	func removeAll()
	{
		self.sections.removeAll()
		self.tableView.reloadData()
	}
	
	
	
	
	
	// MARK: UITableViewDataSource
	
	public override func numberOfSections(in tableView: UITableView) -> Int
	{
		return self.sections.count
	}
	
	public override func tableView(_ tableView: UITableView,
								   cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		let cell = tableView.dequeueReusableCell(of: Cell.self, for: indexPath)
		let element = self.sections[indexPath.section].elements[indexPath.row]
		
		cell.update(with: element)
		
		return cell
	}
	
	
	
	
	
	public override func tableView(_ tableView: UITableView,
				   titleForHeaderInSection section: Int) -> String?
	{
		return self.headerTitleProvider?(self.sections[section].model)
//		if let keyPath = self.headerTitleKeyPath {
//			return "\(self.sections[section].model[keyPath: keyPath])"
//		}
//
//		return nil
	}
	
	public override func tableView(_ tableView: UITableView,
								   titleForFooterInSection section: Int) -> String?
	{
		return self.footerTitleProvider?(self.sections[section].model)
//		if let keyPath = self.footerTitleKeyPath {
//			return "\(self.sections[section].model[keyPath: keyPath])"
//		}
//
//		return nil
	}
	
	public override func tableView(_ tableView: UITableView,
								   numberOfRowsInSection section: Int) -> Int
	{
		return self.sections[section].elements.count
	}
	
	
	
	
	
	// MARK: CustomStringConvertible
	
	override public var description: String {
		return "\(type(of: self)) [\(self.sections.count) sections]"
	}
}



public extension ModelDataSourceProxy
{
	public func bind<T>(to delegateProxy: T) -> Observable<Section.Element?>
		where T: SingleSelectionProxy
	{
		return delegateProxy.selection
//			.unwrapOrComplete()
//			.map({ [unowned self] indexPath in
//				self.sections[indexPath.section].elements[indexPath.row]
//			})
			
			.map({ [unowned self] indexPath -> Section.Element? in
				if let indexPath = indexPath {
					return self.sections[indexPath.section].elements[indexPath.row]
				} else {
					return nil
				}
			})
//			.takeUntil(delegateProxy.rx.deallocated)
//			.takeUntil(delegateProxy.rx.deallocated)
//			.unwrap()
//			.takeUntil(delegateProxy.rx.deallocated)
		//		return Observable.create({ Observable in
		//
		//		})
	}
	
	public func bind<T>(to delegateProxy: T) -> Observable<[Section.Element]?>
		where T: MultipleSelectionProxy
	{
		return delegateProxy.selection
//			.unwrapOrComplete()
//			.unwrap()
			.map({ [unowned self] indexPaths in
				indexPaths?.map({
					self.sections[$0.section].elements[$0.row]
				})
			})
//			.takeUntil(delegateProxy.rx.deallocated)
		
//			.map({ [unowned self] indexPaths -> [Section.Element]? in
//				if let indexPaths = indexPaths {
//					return self.sections[indexPath.section].elements[indexPath.row]
//				} else {
//					return nil
//				}
//			})
		
		//		return Observable.create({ Observable in
		//
		//		})
	}
}




