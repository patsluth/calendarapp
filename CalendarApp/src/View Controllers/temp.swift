//
//  CalendarSelectorViewController.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2018-10-27.
//  Copyright © 2018 Pat Sluth. All rights reserved.
//

import UIKit
import EventKit

import RxSwift
import RxCocoa
import RxSwiftExt
import Permission






public enum IndexedDataProviderIndexType
{
	case Index(Int)
	case IndexPath(IndexPath)
}

public protocol IndexedDataProviderProtocol
{
	
}



//protocol SelectionViewControllerProtocol: UITableViewDataSource, UITableViewDelegate
//	where Self: UIViewController
//{
//	associatedtype Input
//	associatedtype Output
//}





protocol DataProviderSelectionProtocol: UITableViewDelegate
	//where Self: UITableViewDataSource
{
//	associatedtype IndexType
	associatedtype ModelType//: UITableViewModelCellProtocol
	
//	typealias Rows = [CellType.Model]
//
//
//
//
//
//	var tableView: UITableView { get }
//
//
//
//
//
//
//	func row(at index: IndexType) -> Rows.Element
}

class DataProviderSingleSelection<Model>: NSObject, DataProviderSelectionProtocol
{
	typealias ModelType = Model
}

class DataProviderMultipleSelection<Model>: NSObject, DataProviderSelectionProtocol
{
	typealias ModelType = [Model]
}




public protocol UITableViewDataProviderProtocol: UITableViewDataSource
	//where Self: UITableViewDataSource
{
	associatedtype IndexType
	associatedtype CellType: UITableView.ModelCell
	
	typealias Rows = [CellType.Model]
	
	
	
	
	
	var tableView: UITableView { get }
	
	
	
	
	
	
	func row(at index: IndexType) -> Rows.Element
}

protocol AnyUITableViewDataProvider: class
{
	init(tableView: UITableView)
}













//class UITableViewRowDataProvider<Row>: UITableViewDataProvider
//{
//	typealias Rows = [Row]
//
////	func section(at section: Int) -> Sections.Element
////	func row(at indexPath: IndexPath) -> Rows.Element
//
//
//
//
//
//	fileprivate var rows: Rows
//
//
//
//
//
//	required init()
//	{
//		self.rows = Rows()
//	}
//
////	func row(at indexPath: IndexPath) -> Rows.Element
////	{
////		let section = self.section(at: indexPath.section)
////		return section.rows[indexPath.row]
////	}
//}

//extension UITableViewRowDataProvider: RangeReplaceableCollection
//{
//	typealias Index = Sections.Index
//	typealias Element = Sections.Element
//
//	typealias SubSequence = Sections.SubSequence
//
//
//
//
//
//	var startIndex: Index {
//		return self.sections.startIndex
//	}
//	var endIndex: Index {
//		return self.sections.endIndex
//	}
//
//
//
//
//
//	func index(after i: Index) -> Index
//	{
//		return self.sections.index(after: i)
//	}
//
//	func append(_ newElement: Element)
//	{
//		self.sections.append(newElement)
//	}
//
//	func append<S>(contentsOf newElements: S)
//		where S: Sequence, Element == S.Element
//	{
//		self.sections.append(contentsOf: newElements)
//	}
//
//	func insert(_ newElement: Element, at i: Index)
//	{
//		self.sections.insert(newElement, at: i)
//	}
//
//	func remove(at position: Index) -> Element
//	{
//		let element = self.sections.remove(at: position)
//		return element;
//	}
//
//	func removeSubrange(_ bounds: Range<Index>)
//	{
//		self.sections.removeSubrange(bounds)
//	}
//
//	func removeFirst() -> Element
//	{
//		return self.sections.removeFirst()
//	}
//
//	func removeFirst(_ k: Int)
//	{
//		self.sections.removeFirst(k)
//	}
//
//	func removeAll(keepingCapacity keepCapacity: Bool)
//	{
//		self.sections.removeAll(keepingCapacity: keepCapacity)
//	}
//
//	func removeAll(where predicate: (Element) throws -> Bool) rethrows
//	{
//		try self.sections.removeAll(where: predicate)
//	}
//
//	subscript(index: Index) -> Iterator.Element
//	{
//		return self.sections[index]
//	}
//
//	subscript(bounds: Range<Index>) -> SubSequence
//	{
//		return self.sections[bounds]
//	}
//}


//enum IndexPathChanges
//{
//	case Inserted(IndexSet, [IndexPath])
//	case Deleted([IndexPath])
//	case Inserted([IndexPath])
//}


//class SectionTitleProvider<T>
//{
//	typealias <#type name#> = <#type expression#>
//}


//class UITableViewRowProvider<Cell>
//	where Cell: UITableViewDataProviderCell
//{
//	typealias Section = _Section
//	typealias Cell = _Cell
//	typealias Sections = [(Section, Rows)]
//	typealias Rows = [Cell.Value]
//}

open class DataProviderSection<Cell>
	where Cell: UITableView.ModelCell
{
	
}

open class DataProviderRow<Cell>
	where Cell: UITableView.ModelCell
{
	
}

class UITableViewRowDataProvider2<Cell>: NSObject, UITableViewDataProviderProtocol, UITableViewDataSource
	where Cell: UITableView.ModelCell
{
	typealias CellType = Cell
	typealias IndexType = Int
	
	
	
	
	
	fileprivate var rows: Rows
	let tableView: UITableView
	let disposeBag = DisposeBag()
	
	
	
	
	deinit
	{
		print(type(of: self), #function)
	}
	
	required init(tableView: UITableView)
	{
		self.rows = Rows()
		self.tableView = tableView
		
		super.init()
		
//		defer {
		self.tableView.register(Cell.nib, of: Cell.self)
		
		self.tableView.rx.setDataSource(self)
			.disposed(by: self.disposeBag)
//		self.tableView.dataSource = self
			//			self.tableView.delegate = self
//		}
	}
	
	func row(at index: Int) -> Cell.Model
	{
		return self.rows[index]
	}
	
	//	convenience init()
	//	{
	//		self.init()
	//
	//		initBlock(self)
	//	}
	
	
	//	func numberOfRows() -> Rows.Index
	//	{
	//		return self.rows.count
	//	}
	//	=
	
	
	
	//@discardableResult
	func append(_ row: Rows.Element,
				with animation: UITableView.RowAnimation = .none)// -> Rows.Index
	{
		self.insert(row, at: self.rows.endIndex, with: animation)
		//		self.rows.append(row)
		//return self.rows.endIndex.advanced(by: -1)
	}
	
	func append(_ rows: [Rows.Element],
				with animation: UITableView.RowAnimation = .none)// -> Rows.Index
	{
		var indexPaths = [IndexPath]()
		for (index, row) in rows.enumerated() {
			let indexPath = IndexPath(row: self.rows.endIndex + index, section: 0)
			self.rows.insert(row, at: indexPath.row)
			indexPaths.append(indexPath)
		}
		
		self.tableView.insertRows(at: indexPaths, with: animation)
	}
	
	//	@discardableResult
	//	func append(rows: Rows) -> [Rows.Index]
	//	{
	//		let section = Sections.Element(value: value, rows: rows)
	//		return self.append(section: section)
	//	}
	
	//	@discardableResult
	//	func append(section value: Section, rows: Row...) -> Sections.Index
	//	{
	//		return self.append(section: section, rows: rows)
	//	}
	
	func insert(_ row: Rows.Element,
				at index: IndexType,
				with animation: UITableView.RowAnimation = .none)
	{
		let indexPath = IndexPath(row: index, section: 0)
		self.rows.insert(row, at: indexPath.row)
		
		self.tableView.insertRows(at: [indexPath], with: animation)
		//		return index
	}
	
	@discardableResult
	func remove(at index: IndexType,
				with animation: UITableView.RowAnimation = .none) -> Rows.Element
	{
		let indexPath = IndexPath(row: index, section: 0)
		let row = self.rows.remove(at: indexPath.row)
		
		self.tableView.deleteRows(at: [indexPath], with: animation)
		
		return row
	}
	
	func removeAll()
	{
		self.rows.removeAll()
		self.tableView.reloadData()
	}
	
	
	
	
	
	// MARK: UITableViewDataSource
	
	func numberOfSections(in tableView: UITableView) -> Int
	{
		return 1
	}
	
	func tableView(_ tableView: UITableView,
				   numberOfRowsInSection section: Int) -> Int
	{
		return self.rows.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		let cell = tableView.dequeueReusableCell(of: Cell.self, for: indexPath)
		
		cell.update(with: self.row(at: indexPath.row))
		
		return cell
	}
	
	
	
	
	
	//	// MARK: UITableViewDelegate
	//
	//	func tableView(_ tableView: UITableView,
	//				   estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
	//	{
	//		return tableView.estimatedRowHeight
	//	}
	//
	//	func tableView(_ tableView: UITableView,
	//				   heightForRowAt indexPath: IndexPath) -> CGFloat
	//	{
	//		return tableView.rowHeight
	//	}
	//
	//	func tableView(_ tableView: UITableView,
	//				   willDisplay cell: UITableViewCell,
	//				   forRowAt indexPath: IndexPath)
	//	{
	//	}
	//
	//	func tableView(_ tableView: UITableView,
	//				   didEndDisplaying cell: UITableViewCell,
	//				   forRowAt indexPath: IndexPath)
	//	{
	//		cell.prepareForReuse()
	//	}
	//
	//	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	//	{
	//		let cell = tableView.cellForRow(of: Cell.self, at: indexPath)!
	//		let row = self.row(at: indexPath)
	//
	//		self.selection.accept(row)
	//	}
	
	
	
	
	
	// MARK: CustomStringConvertible
	
	override var description: String {
		return "\(type(of: self)) [\(self.rows.count) rows]"
	}
}











class UITableViewRowDataProvider<Cell>: NSObject, UITableViewDataProviderProtocol, AnyUITableViewDataProvider
	where Cell: UITableView.ModelCell
{
	typealias CellType = Cell
	typealias IndexType = Int
	
	
	deinit
	{
		print(type(of: self), #function)
	}
	
	
	fileprivate var rows = Rows()
	
//	lazy var viewController: UIViewController = {
//		let view: UIView = {
//			let view = UIView()
//			view.backgroundColor = UIColor.white
//			return view
//		}()
//
//		let viewController = UIViewController()
//		viewController.view = view
//
////		let tableView = UITableView()
////		tableView.translatesAutoresizingMaskIntoConstraints = false
////		tableView.dataSource = self
////		tableView.register(Cell.nib, of: Cell.self)
////		tableView.delegate = self
//
//		viewController.view.addSubview(self.tableView)
//
//		NSLayoutConstraint.activate([
//			self.tableView.topAnchor.constraint(equalTo: view.safeTopAnchor),
//			self.tableView.leftAnchor.constraint(equalTo: view.safeLeftAnchor),
//			self.tableView.bottomAnchor.constraint(equalTo: view.safeBottomAnchor),
//			self.tableView.rightAnchor.constraint(equalTo: view.safeRightAnchor),
//			])
//
//		return viewController
//	}()
//
//	lazy var tableView: UITableView = {
//		let tableView = UITableView()
//		tableView.translatesAutoresizingMaskIntoConstraints = false
//		tableView.dataSource = self
//		tableView.register(Cell.nib, of: Cell.self)
//		return tableView
//	}()
	
	let tableView: UITableView
//	var tableView: UITableView? = nil {
//		didSet
//		{
//			self.disposeBag = DisposeBag()
//
//			self.tableView?.register(Cell.nib, of: Cell.self)
//
//			self.tableView?.rx.setDataSource(self)
//				.disposed(by: self.disposeBag)
//
//			self.tableView?.reloadData()
//		}
//	}
	private let disposeBag = DisposeBag()
	
	
	
	
	
	
	required init(tableView: UITableView)
	{
//		self.rows = Rows()
		self.tableView = tableView

		super.init()

		self.tableView.register(Cell.nib, of: Cell.self)
		
		self.tableView.rx.setDataSource(self)
			.disposed(by: self.disposeBag)
	}
	
	func row(at index: Int) -> Cell.Model
	{
		return self.rows[index]
	}
	
	//	convenience init()
	//	{
	//		self.init()
	//
	//		initBlock(self)
	//	}
	
	
//	func numberOfRows() -> Rows.Index
//	{
//		return self.rows.count
//	}
//	=
	
	
	
	//@discardableResult
	func append(_ row: Rows.Element,
				with animation: UITableView.RowAnimation = .none)// -> Rows.Index
	{
		self.insert(row, at: self.rows.endIndex, with: animation)
//		self.rows.append(row)
		//return self.rows.endIndex.advanced(by: -1)
	}
	
	func append(_ rows: [Rows.Element],
				with animation: UITableView.RowAnimation = .none)// -> Rows.Index
	{
		var indexPaths = [IndexPath]()
		for (index, row) in rows.enumerated() {
			let indexPath = IndexPath(row: self.rows.endIndex + index, section: 0)
			self.rows.insert(row, at: indexPath.row)
			indexPaths.append(indexPath)
		}
		
		self.tableView.insertRows(at: indexPaths, with: animation)
	}
	
//	@discardableResult
//	func append(rows: Rows) -> [Rows.Index]
//	{
//		let section = Sections.Element(value: value, rows: rows)
//		return self.append(section: section)
//	}
	
	//	@discardableResult
	//	func append(section value: Section, rows: Row...) -> Sections.Index
	//	{
	//		return self.append(section: section, rows: rows)
	//	}
	
	func insert(_ row: Rows.Element,
				at index: IndexType,
				with animation: UITableView.RowAnimation = .none)
	{
		let indexPath = IndexPath(row: index, section: 0)
		self.rows.insert(row, at: indexPath.row)
		
		self.tableView.insertRows(at: [indexPath], with: animation)
//		return index
	}
	
	@discardableResult
	func remove(at index: IndexType,
				with animation: UITableView.RowAnimation = .none) -> Rows.Element
	{
		let indexPath = IndexPath(row: index, section: 0)
		let row = self.rows.remove(at: indexPath.row)
		
		self.tableView.deleteRows(at: [indexPath], with: animation)
		
		return row
	}
	
	func removeAll()
	{
		self.rows.removeAll()
		self.tableView.reloadData()
	}
	
	
	
	
	
	// MARK: UITableViewDataSource
	
	func numberOfSections(in tableView: UITableView) -> Int
	{
		return 1
	}
	
	func tableView(_ tableView: UITableView,
				   numberOfRowsInSection section: Int) -> Int
	{
		return self.rows.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		let cell = tableView.dequeueReusableCell(of: Cell.self, for: indexPath)
		
		cell.update(with: self.row(at: indexPath.row))
		
		return cell
	}
	
	
	
	
	
	//	// MARK: UITableViewDelegate
	//
	//	func tableView(_ tableView: UITableView,
	//				   estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
	//	{
	//		return tableView.estimatedRowHeight
	//	}
	//
	//	func tableView(_ tableView: UITableView,
	//				   heightForRowAt indexPath: IndexPath) -> CGFloat
	//	{
	//		return tableView.rowHeight
	//	}
	//
	//	func tableView(_ tableView: UITableView,
	//				   willDisplay cell: UITableViewCell,
	//				   forRowAt indexPath: IndexPath)
	//	{
	//	}
	//
	//	func tableView(_ tableView: UITableView,
	//				   didEndDisplaying cell: UITableViewCell,
	//				   forRowAt indexPath: IndexPath)
	//	{
	//		cell.prepareForReuse()
	//	}
	//
	//	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	//	{
	//		let cell = tableView.cellForRow(of: Cell.self, at: indexPath)!
	//		let row = self.row(at: indexPath)
	//
	//		self.selection.accept(row)
	//	}
	
	
	
	
	
	// MARK: CustomStringConvertible
	
	override var description: String {
		return "\(type(of: self)) [\(self.rows.count) rows]"
	}
}










//open class UITableViewDelegateProxy<Cell>: UITableViewRowDataProvider<Cell>, UITableViewDelegate
//	where Cell: UITableViewModelCellProtocol
//{
//	weak var originalDelegate: UITableViewDelegate?
//b
//	required init(tableView: UITableView)
//	{
//		super.init(tableView: tableView)
//
//		self.tableView.allowsMultipleSelection = true
//
//		self.originalDelegate = tableView.delegate
//		self.tableView.delegate = nil
//		self.tableView.rx.delegate.setForwardToDelegate(self, retainDelegate: false)
//	}
//}












class RowDataProviderSingleSelection<Cell>: UITableViewRowDataProvider<Cell>, UITableViewDelegate
	where Cell: UITableView.ModelCell
{
	required init(tableView: UITableView)
	{
		super.init(tableView: tableView)
		
		self.tableView.rx.delegate.setForwardToDelegate(self, retainDelegate: false)
	}
}





class RowDataProviderMultipleSelection<Cell>: UITableViewRowDataProvider<Cell>, UITableViewDelegate
	where Cell: UITableView.ModelCell
{
	weak var originalDelegate: UITableViewDelegate?
	
	required init(tableView: UITableView)
	{
		super.init(tableView: tableView)
		
		self.tableView.allowsMultipleSelection = true
		
		self.originalDelegate = tableView.delegate
		self.tableView.delegate = nil
		self.tableView.rx.delegate.setForwardToDelegate(self, retainDelegate: false)
	}
	
	func tableView(_ tableView: UITableView,
				   willDisplay cell: UITableViewCell,
				   forRowAt indexPath: IndexPath)
	{
		if tableView.indexPathsForSelectedRows?.contains(indexPath) ?? false {
			cell.accessoryType = UITableViewCell.AccessoryType.checkmark
		} else {
			cell.accessoryType = UITableViewCell.AccessoryType.none
		}
	}
	
	func tableView(_ tableView: UITableView,
				   didEndDisplaying cell: UITableViewCell,
				   forRowAt indexPath: IndexPath)
	{
		self.originalDelegate?.tableView?(tableView, didEndDisplaying: cell, forRowAt: indexPath)
		
		cell.accessoryType = UITableViewCell.AccessoryType.none
	}
	
	func tableView(_ tableView: UITableView,
				   didDeselectRowAt indexPath: IndexPath)
	{
		let cell = tableView.cellForRow(at: indexPath)
		cell?.accessoryType = UITableViewCell.AccessoryType.none
	}
	
	func tableView(_ tableView: UITableView,
				   didSelectRowAt indexPath: IndexPath)
	{
		let cell = tableView.cellForRow(at: indexPath)
		cell?.accessoryType = UITableViewCell.AccessoryType.checkmark
	}
}




protocol AnyRowData
{
	var sectionType: Any.Type { get }
	var cellType: Any.Type { get }
	
//	var sectionType: Any.Type { get }
//	var cellType: UITableViewModelCellProtocol.Type { get }
}

//enum DataSource
//{
//	class SingleSection
//	{
//
//	}
//
//	class MultipleSection
//	{
//
//	}
//}

class RowData<Section, Cell>: AnyRowData
	where Cell: UITableView.ModelCell
{
	let sectionType: Any.Type = Section.self
	let cellType: Any.Type = Cell.self
	
//	typealias CellType = Cell
//	let sectionType = Section.Type
//	let cellType: Cell.Type
	
//	typealias CellType = Cell
//	typealias IndexType = IndexPath
//	typealias Sections = [(Section, Rows)]
	typealias Rows = [Cell.Model]
	
//	typealias SectionTitleProvider = (Section) -> String?
	
	init(title: String)
	{
		
	}
}













class IndexedDataModel
{
	
}




class UITableViewDataProvider<Section, Cell>: NSObject, UITableViewDataProviderProtocol, AnyUITableViewDataProvider
	where Cell: UITableView.ModelCell
{
	typealias CellType = Cell
	typealias IndexType = IndexPath
	typealias Sections = [(Section, Rows)]
	
	typealias SectionTitleProvider = (Section) -> String?
//	typealias CellSelected = (Cell, Cell.Value) -> Void
	
//	enum SectionChanges
//	{
//		case Inserted(IndexSet)
//		case Moved(IndexSet)
//		case Deleted(IndexSet)
//	}
//
//	enum RowChanges
//	{
//		case Inserted(IndexSet)
//		case Moved(IndexSet)
//		case Deleted(IndexSet)
//	}
	
	
	
	deinit
	{
		print(type(of: self), #function)
	}
	
	fileprivate var sections = Sections()
	
	
	let tableView: UITableView
	private let disposeBag = DisposeBag()
//	var selection: BehaviorRelay<Cell.Value?>(value: nil)? = nil
	
	var sectionTitleProvider: SectionTitleProvider! = nil
	//var cellSelected: CellSelected! = nil
//	typealias SectionTitleProvider = (Section) -> String
//	var sectionTitleKeyPath: SectionTitleProvider? = nil
	
	
	
	
	
	
	required init(tableView: UITableView)
	{
		self.tableView = tableView
//		self.selection = selection

		super.init()

//		defer {
		self.tableView.register(Cell.nib, of: Cell.self)

		self.tableView.rx.setDataSource(self)
			.disposed(by: self.disposeBag)
//		self.tableView.rx.setDelegate(self)
//			.disposed(by: self.disposeBag)
//			self.tableView.dataSource = self

//			self.tableView.delegate = self
//		}
	}
	
	func row(at index: IndexPath) -> Cell.Model
	{
		return self.sections[index.section].1[index.row]
	}
	
//	convenience init()
//	{
//		self.init()
//
//		initBlock(self)
//	}
	
	
	
	
	func append(_ section: Sections.Element,
				with animation: UITableView.RowAnimation = .none)// -> Rows.Index
	{
		self.insert(section, at: self.sections.endIndex, with: animation)
//		self.insert(row, at: self.sections.endIndex, with: animation)
		//		self.rows.append(row)
		//return self.rows.endIndex.advanced(by: -1)
	}
	
//	func append(_ row: Rows.Element,
//				with animation: UITableView.RowAnimation = .none)// -> Rows.Index
//	{
//		self.insert(row, at: self.rows.endIndex, with: animation)
//		//		self.rows.append(row)
//		//return self.rows.endIndex.advanced(by: -1)
//	}
	
	//	@discardableResult
	//	func append(rows: Rows) -> [Rows.Index]
	//	{
	//		let section = Sections.Element(value: value, rows: rows)
	//		return self.append(section: section)
	//	}
	
	//	@discardableResult
	//	func append(section value: Section, rows: Row...) -> Sections.Index
	//	{
	//		return self.append(section: section, rows: rows)
	//	}
	
	func insert(_ section: Sections.Element,
				at index: Int,
				with animation: UITableView.RowAnimation = .none)
	{
		self.sections.insert(section, at: index)
		
		self.tableView.insertSections(IndexSet(integer: index), with: animation)
	}
	
	@discardableResult
	func remove(at index: Int,
				with animation: UITableView.RowAnimation = .none) -> Sections.Element
	{
		let section = self.sections.remove(at: index)
		
		self.tableView.deleteSections(IndexSet(integer: index), with: animation)
		
		return section
	}
	
	
	
	
	func insert(_ row: Rows.Element,
				at indexPath: IndexPath,
				with animation: UITableView.RowAnimation = .none)
	{
		self.sections[indexPath.section].1.insert(row, at: indexPath.row)
		
		self.tableView.insertRows(at: [indexPath], with: animation)
//		self.tableView.insertRows(at: [indexPath], with: animation)
		//		return index
	}
	
	@discardableResult
	func remove(at index: IndexType,
				with animation: UITableView.RowAnimation = .none) -> Rows.Element
	{
		let row = self.sections[index.section].1.remove(at: index.row)
		
		self.tableView.deleteRows(at: [index], with: animation)
		
		return row
	}
	
	func removeAll()
	{
		self.sections.removeAll()
		self.tableView.reloadData()
	}
	
	
	
	
	
	// MARK: UITableViewDataSource
	
	func numberOfSections(in tableView: UITableView) -> Int
	{
		return self.sections.count
	}
	
	func tableView(_ tableView: UITableView,
				   titleForHeaderInSection section: Int) -> String?
	{
//		return tableView.dataSource?.tableView?(tableView, titleForHeaderInSection: section)
		return self.sectionTitleProvider?(self.sections[section].0)
	}
	
//	func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String?
//	{
//		return tableView.dataSource?.tableView?(tableView, titleForFooterInSection: section)
//	}
	
	func tableView(_ tableView: UITableView,
				   numberOfRowsInSection section: Int) -> Int
	{
		return self.sections[section].1.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		let cell = tableView.dequeueReusableCell(of: Cell.self, for: indexPath)
		
		cell.update(with: self.row(at: indexPath))
		
		return cell
	}
	
	
	
	
	
	//	// MARK: UITableViewDelegate
	//
	//	func tableView(_ tableView: UITableView,
	//				   estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
	//	{
	//		return tableView.estimatedRowHeight
	//	}
	//
	//	func tableView(_ tableView: UITableView,
	//				   heightForRowAt indexPath: IndexPath) -> CGFloat
	//	{
	//		return tableView.rowHeight
	//	}
	//
	//	func tableView(_ tableView: UITableView,
	//				   willDisplay cell: UITableViewCell,
	//				   forRowAt indexPath: IndexPath)
	//	{
	//	}
	//
	//	func tableView(_ tableView: UITableView,
	//				   didEndDisplaying cell: UITableViewCell,
	//				   forRowAt indexPath: IndexPath)
	//	{
	//		cell.prepareForReuse()
	//	}
	//
	//	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	//	{
	//		let cell = tableView.cellForRow(of: Cell.self, at: indexPath)!
	//		let row = self.row(at: indexPath)
	//
	//		self.selection.accept(row)
	//	}
	
	
	
	
	
	// MARK: CustomStringConvertible
	
	override var description: String {
		return "\(type(of: self)) [\(self.sections.count) sections]"
	}
	
	
	
	
	
	
//	func numberOfSections() -> Sections.Index
//	{
//		return self.sections.count
//	}
//
//	func numberOfRows(in section: Sections.Index) -> Sections.Index
//	{
//		let section = self.section(at: section)
//		return section.1.count
//	}
//
//	func section(at section: Sections.Index) -> Sections.Element
//	{
//		return self.sections[section]
//	}
//
//	func row(at indexPath: IndexPath) -> Rows.Element
//	{
//		let section = self.section(at: indexPath.section)
//		return section.1[indexPath.row]
//	}
//
//
//
//	@discardableResult
//	func append(section: Sections.Element) -> Sections.Index
//	{
//		self.sections.append(section)
//		return self.sections.endIndex.advanced(by: -1)
//	}
//
//	@discardableResult
//	func append(section value: Section, rows: Rows) -> Sections.Index
//	{
//		let section = Sections.Element(value: value, rows: rows)
//		return self.append(section: section)
//	}
//
////	@discardableResult
////	func append(section value: Section, rows: Row...) -> Sections.Index
////	{
////		return self.append(section: section, rows: rows)
////	}
//
//	@discardableResult
//	func insert(section: Sections.Element, at index: Sections.Index) -> Sections.Index
//	{
//		let index = self.sections.indices.clamp(index)
//		self.sections.insert(section, at: index)
//		return index
//	}
//
//	@discardableResult
//	func insert(row: Rows.Element, at indexPath: IndexPath) -> IndexPath
//	{
//		self.sections[indexPath.section].1.insert(row, at: indexPath.row)
//		return indexPath
//	}
//
//	@discardableResult
//	func remove(sectionAt index: Sections.Index) -> Sections.Index
//	{
//		self.sections.remove(at: index)
//		return index
//	}
//
//	@discardableResult
//	func remove(rowAt indexPath: IndexPath) -> IndexPath
//	{
//		self.sections[indexPath.section].1.remove(at: indexPath.row)
//		return indexPath
//	}
//
//	func removeAll()
//	{
//		self.sections.removeAll()
//	}
//
//	func removeAll(rowsInSection index: Sections.Index)
//	{
//		self.sections[index].1.removeAll()
//	}
//
////	@discardableResult
////	func append(row: Rows.Element, in section: Sections.Index) -> IndexPath
////	{
////		self.sections.append(section)
////		return self.sections.endIndex.advanced(by: -1)
////	}
////
////	@discardableResult
////	func insert(row: Sections.Element, at index: Sections.Index) -> IndexPath
////	{
////		let index = self.sections.indices.clamp(index)
////		self.sections.insert(section, at: index)
////		return index
////	}
////
////	@discardableResult
////	func removeSection(section: Sections.Element, at index: Sections.Index) -> Sections.Index
////	{
////		let index = self.sections.indices.clamp(index) - 1b
////		self.sections.remove(at: index)
////		return index
////	}
//
//
//
//
//	// MARK: UITableViewDataSource
//
//	func numberOfSections(in tableView: UITableView) -> Int
//	{
//		return self.numberOfSections()
//	}
//
//	func tableView(_ tableView: UITableView,
//				   titleForHeaderInSection section: Int) -> String?
//	{
//		let section = self.section(at: section)
//
//		return self.sectionTitleProvider?(section.0)
//	}
//
//	func tableView(_ tableView: UITableView,
//				   numberOfRowsInSection section: Int) -> Int
//	{
//		return self.numberOfRows(in: section)
//	}
//
//	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
//	{
//		let cell = tableView.dequeueReusableCell(of: Cell.self, for: indexPath)
//		let row = self.row(at: indexPath)
//
//		cell.update(with: row)
//
//		return cell
//	}
//
//
//
//
////	// MARK: UITableViewDelegate
////
////	func tableView(_ tableView: UITableView,
////				   estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
////	{
////		return tableView.estimatedRowHeight
////	}
////
////	func tableView(_ tableView: UITableView,
////				   heightForRowAt indexPath: IndexPath) -> CGFloat
////	{
////		return tableView.rowHeight
////	}
////
////	func tableView(_ tableView: UITableView,
////				   willDisplay cell: UITableViewCell,
////				   forRowAt indexPath: IndexPath)
////	{
////	}
////
////	func tableView(_ tableView: UITableView,
////				   didEndDisplaying cell: UITableViewCell,
////				   forRowAt indexPath: IndexPath)
////	{
////		cell.prepareForReuse()
////	}
////
////	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
////	{
////		let cell = tableView.cellForRow(of: Cell.self, at: indexPath)!
////		let row = self.row(at: indexPath)
////
////		self.selection.accept(row)
////	}
}









//class UITableViewDataProvider2<Section, Cell>: UITableView.Proxy.DataSource//, UITableViewDataProviderProtocol, AnyUITableViewDataProvider
//	where Cell: UITableViewModelCellProtocol
//{
//	typealias CellType = Cell
//	typealias IndexType = IndexPath
//	typealias Sections = [(Section, Rows)]
//	typealias Rows = [CellType.Model]
//
//	typealias SectionTitleProvider = (Section) -> String?
//	//	typealias CellSelected = (Cell, Cell.Value) -> Void
//
//	//	enum SectionChanges
//	//	{
//	//		case Inserted(IndexSet)
//	//		case Moved(IndexSet)
//	//		case Deleted(IndexSet)
//	//	}
//	//
//	//	enum RowChanges
//	//	{
//	//		case Inserted(IndexSet)
//	//		case Moved(IndexSet)
//	//		case Deleted(IndexSet)
//	//	}
//
//
//
//
//
//	let tableView: UITableView
//	fileprivate var sections = Sections()
//	private let disposeBag = DisposeBag()
//	//	var selection: BehaviorRelay<Cell.Value?>(value: nil)? = nil
//
//	var sectionTitleProvider: SectionTitleProvider! = nil
//	//var cellSelected: CellSelected! = nil
//	//	typealias SectionTitleProvider = (Section) -> String
//	//	var sectionTitleKeyPath: SectionTitleProvider? = nil
//
//
//
//
//
//
//	required public init(_ tableView: UITableView)
//	{
//		self.tableView = tableView
//
//		self.tableView.register(Cell.nib, of: Cell.self)
//		//		self.selection = selection
//
//		super.init(self.tableView)
//
//		//		defer {
//
//
////		self.tableView.rx.setDataSource(self)
////			.disposed(by: self.disposeBag)
//		//		self.tableView.rx.setDelegate(self)
//		//			.disposed(by: self.disposeBag)
//		//			self.tableView.dataSource = self
//
//		//			self.tableView.delegate = self
//		//		}
//	}
//
//	func row(at index: IndexPath) -> Cell.Model
//	{
//		return self.sections[index.section].1[index.row]
//	}
//
//	//	convenience init()
//	//	{
//	//		self.init()
//	//
//	//		initBlock(self)
//	//	}
//
//
//
//
//	func append(_ section: Sections.Element,
//				with animation: UITableView.RowAnimation = .none)// -> Rows.Index
//	{
//		self.insert(section, at: self.sections.endIndex, with: animation)
//		//		self.insert(row, at: self.sections.endIndex, with: animation)
//		//		self.rows.append(row)
//		//return self.rows.endIndex.advanced(by: -1)
//	}
//
//	//	func append(_ row: Rows.Element,
//	//				with animation: UITableView.RowAnimation = .none)// -> Rows.Index
//	//	{
//	//		self.insert(row, at: self.rows.endIndex, with: animation)
//	//		//		self.rows.append(row)
//	//		//return self.rows.endIndex.advanced(by: -1)
//	//	}
//
//	//	@discardableResult
//	//	func append(rows: Rows) -> [Rows.Index]
//	//	{
//	//		let section = Sections.Element(value: value, rows: rows)
//	//		return self.append(section: section)
//	//	}
//
//	//	@discardableResult
//	//	func append(section value: Section, rows: Row...) -> Sections.Index
//	//	{
//	//		return self.append(section: section, rows: rows)
//	//	}
//
//	func insert(_ section: Sections.Element,
//				at index: Int,
//				with animation: UITableView.RowAnimation = .none)
//	{
//		self.sections.insert(section, at: index)
//
//		self.tableView.insertSections(IndexSet(integer: index), with: animation)
//	}
//
//	@discardableResult
//	func remove(at index: Int,
//				with animation: UITableView.RowAnimation = .none) -> Sections.Element
//	{
//		let section = self.sections.remove(at: index)
//
//		self.tableView.deleteSections(IndexSet(integer: index), with: animation)
//
//		return section
//	}
//
//
//
//
//	func insert(_ row: Rows.Element,
//				at indexPath: IndexPath,
//				with animation: UITableView.RowAnimation = .none)
//	{
//		self.sections[indexPath.section].1.insert(row, at: indexPath.row)
//
//		self.tableView.insertRows(at: [indexPath], with: animation)
//		//		self.tableView.insertRows(at: [indexPath], with: animation)
//		//		return index
//	}
//
//	@discardableResult
//	func remove(at index: IndexType,
//				with animation: UITableView.RowAnimation = .none) -> Rows.Element
//	{
//		let row = self.sections[index.section].1.remove(at: index.row)
//
//		self.tableView.deleteRows(at: [index], with: animation)
//
//		return row
//	}
//
//	func removeAll()
//	{
//		self.sections.removeAll()
//		self.tableView.reloadData()
//	}
//
//
//
//
//
//	// MARK: UITableViewDataSource
//
//	override func numberOfSections(in tableView: UITableView) -> Int
//	{
//		return self.sections.count
//	}
//
//	override func tableView(_ tableView: UITableView,
//				   titleForHeaderInSection section: Int) -> String?
//	{
//		//		return tableView.dataSource?.tableView?(tableView, titleForHeaderInSection: section)
//		return self.sectionTitleProvider?(self.sections[section].0)
//	}
//
//	//	func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String?
//	//	{
//	//		return tableView.dataSource?.tableView?(tableView, titleForFooterInSection: section)
//	//	}
//
//	override func tableView(_ tableView: UITableView,
//				   numberOfRowsInSection section: Int) -> Int
//	{
//		return self.sections[section].1.count
//	}
//
//	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
//	{
//		let cell = tableView.dequeueReusableCell(of: Cell.self, for: indexPath)
//
//		cell.update(with: self.row(at: indexPath))
//
//		return cell
//	}
//
//
//
//
//
//	//	// MARK: UITableViewDelegate
//	//
//	//	func tableView(_ tableView: UITableView,
//	//				   estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
//	//	{
//	//		return tableView.estimatedRowHeight
//	//	}
//	//
//	//	func tableView(_ tableView: UITableView,
//	//				   heightForRowAt indexPath: IndexPath) -> CGFloat
//	//	{
//	//		return tableView.rowHeight
//	//	}
//	//
//	//	func tableView(_ tableView: UITableView,
//	//				   willDisplay cell: UITableViewCell,
//	//				   forRowAt indexPath: IndexPath)
//	//	{
//	//	}
//	//
//	//	func tableView(_ tableView: UITableView,
//	//				   didEndDisplaying cell: UITableViewCell,
//	//				   forRowAt indexPath: IndexPath)
//	//	{
//	//		cell.prepareForReuse()
//	//	}
//	//
//	//	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
//	//	{
//	//		let cell = tableView.cellForRow(of: Cell.self, at: indexPath)!
//	//		let row = self.row(at: indexPath)
//	//
//	//		self.selection.accept(row)
//	//	}
//
//
//
//
//
//	// MARK: CustomStringConvertible
//
//	override var description: String {
//		return "\(type(of: self)) [\(self.sections.count) sections]"
//	}
//
//
//
//
//
//
//	//	func numberOfSections() -> Sections.Index
//	//	{
//	//		return self.sections.count
//	//	}
//	//
//	//	func numberOfRows(in section: Sections.Index) -> Sections.Index
//	//	{
//	//		let section = self.section(at: section)
//	//		return section.1.count
//	//	}
//	//
//	//	func section(at section: Sections.Index) -> Sections.Element
//	//	{
//	//		return self.sections[section]
//	//	}
//	//
//	//	func row(at indexPath: IndexPath) -> Rows.Element
//	//	{
//	//		let section = self.section(at: indexPath.section)
//	//		return section.1[indexPath.row]
//	//	}
//	//
//	//
//	//
//	//	@discardableResult
//	//	func append(section: Sections.Element) -> Sections.Index
//	//	{
//	//		self.sections.append(section)
//	//		return self.sections.endIndex.advanced(by: -1)
//	//	}
//	//
//	//	@discardableResult
//	//	func append(section value: Section, rows: Rows) -> Sections.Index
//	//	{
//	//		let section = Sections.Element(value: value, rows: rows)
//	//		return self.append(section: section)
//	//	}
//	//
//	////	@discardableResult
//	////	func append(section value: Section, rows: Row...) -> Sections.Index
//	////	{
//	////		return self.append(section: section, rows: rows)
//	////	}
//	//
//	//	@discardableResult
//	//	func insert(section: Sections.Element, at index: Sections.Index) -> Sections.Index
//	//	{
//	//		let index = self.sections.indices.clamp(index)
//	//		self.sections.insert(section, at: index)
//	//		return index
//	//	}
//	//
//	//	@discardableResult
//	//	func insert(row: Rows.Element, at indexPath: IndexPath) -> IndexPath
//	//	{
//	//		self.sections[indexPath.section].1.insert(row, at: indexPath.row)
//	//		return indexPath
//	//	}
//	//
//	//	@discardableResult
//	//	func remove(sectionAt index: Sections.Index) -> Sections.Index
//	//	{
//	//		self.sections.remove(at: index)
//	//		return index
//	//	}
//	//
//	//	@discardableResult
//	//	func remove(rowAt indexPath: IndexPath) -> IndexPath
//	//	{
//	//		self.sections[indexPath.section].1.remove(at: indexPath.row)
//	//		return indexPath
//	//	}
//	//
//	//	func removeAll()
//	//	{
//	//		self.sections.removeAll()
//	//	}
//	//
//	//	func removeAll(rowsInSection index: Sections.Index)
//	//	{
//	//		self.sections[index].1.removeAll()
//	//	}
//	//
//	////	@discardableResult
//	////	func append(row: Rows.Element, in section: Sections.Index) -> IndexPath
//	////	{
//	////		self.sections.append(section)
//	////		return self.sections.endIndex.advanced(by: -1)
//	////	}
//	////
//	////	@discardableResult
//	////	func insert(row: Sections.Element, at index: Sections.Index) -> IndexPath
//	////	{
//	////		let index = self.sections.indices.clamp(index)
//	////		self.sections.insert(section, at: index)
//	////		return index
//	////	}
//	////
//	////	@discardableResult
//	////	func removeSection(section: Sections.Element, at index: Sections.Index) -> Sections.Index
//	////	{
//	////		let index = self.sections.indices.clamp(index) - 1b
//	////		self.sections.remove(at: index)
//	////		return index
//	////	}
//	//
//	//
//	//
//	//
//	//	// MARK: UITableViewDataSource
//	//
//	//	func numberOfSections(in tableView: UITableView) -> Int
//	//	{
//	//		return self.numberOfSections()
//	//	}
//	//
//	//	func tableView(_ tableView: UITableView,
//	//				   titleForHeaderInSection section: Int) -> String?
//	//	{
//	//		let section = self.section(at: section)
//	//
//	//		return self.sectionTitleProvider?(section.0)
//	//	}
//	//
//	//	func tableView(_ tableView: UITableView,
//	//				   numberOfRowsInSection section: Int) -> Int
//	//	{
//	//		return self.numberOfRows(in: section)
//	//	}
//	//
//	//	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
//	//	{
//	//		let cell = tableView.dequeueReusableCell(of: Cell.self, for: indexPath)
//	//		let row = self.row(at: indexPath)
//	//
//	//		cell.update(with: row)
//	//
//	//		return cell
//	//	}
//	//
//	//
//	//
//	//
//	////	// MARK: UITableViewDelegate
//	////
//	////	func tableView(_ tableView: UITableView,
//	////				   estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
//	////	{
//	////		return tableView.estimatedRowHeight
//	////	}
//	////
//	////	func tableView(_ tableView: UITableView,
//	////				   heightForRowAt indexPath: IndexPath) -> CGFloat
//	////	{
//	////		return tableView.rowHeight
//	////	}
//	////
//	////	func tableView(_ tableView: UITableView,
//	////				   willDisplay cell: UITableViewCell,
//	////				   forRowAt indexPath: IndexPath)
//	////	{
//	////	}
//	////
//	////	func tableView(_ tableView: UITableView,
//	////				   didEndDisplaying cell: UITableViewCell,
//	////				   forRowAt indexPath: IndexPath)
//	////	{
//	////		cell.prepareForReuse()
//	////	}
//	////
//	////	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
//	////	{
//	////		let cell = tableView.cellForRow(of: Cell.self, at: indexPath)!
//	////		let row = self.row(at: indexPath)
//	////
//	////		self.selection.accept(row)
//	////	}
//}



//enum UITableViewSectionDataProviderIndex<S, R>
//	//where T: UITableViewDataProviderProtocol
//{
//	case Section
//	case Row
//}





//extension UITableViewSectionDataProvider: RangeReplaceableCollection
//{
//	typealias Index = Sections.Index
//	typealias Element = Sections.Element
//
//	typealias SubSequence = Sections.SubSequence
//
//
//
//
//
//	var startIndex: Index {
//		return self.sections.startIndex
//	}
//	var endIndex: Index {
//		return self.sections.endIndex
//	}
//
//
//
//
//
//	func index(after i: Index) -> Index
//	{
//		return self.sections.index(after: i)
//	}
//
//	func append(_ newElement: Element)
//	{
//		self.sections.append(newElement)
//	}
//
//	func append<S>(contentsOf newElements: S)
//		where S: Sequence, Element == S.Element
//	{
//		self.sections.append(contentsOf: newElements)
//	}
//
//	func insert(_ newElement: Element, at i: Index)
//	{
//		self.sections.insert(newElement, at: i)
//	}
//
//	func remove(at position: Index) -> Element
//	{
//		let element = self.sections.remove(at: position)
//		return element;
//	}
//
//	func removeSubrange(_ bounds: Range<Index>)
//	{
//		self.sections.removeSubrange(bounds)
//	}
//
//	func removeFirst() -> Element
//	{
//		return self.sections.removeFirst()
//	}
//
//	func removeFirst(_ k: Int)
//	{
//		self.sections.removeFirst(k)
//	}
//
//	func removeAll(keepingCapacity keepCapacity: Bool)
//	{
//		self.sections.removeAll(keepingCapacity: keepCapacity)
//	}
//
//	func removeAll(where predicate: (Element) throws -> Bool) rethrows
//	{
//		try self.sections.removeAll(where: predicate)
//	}
//
//	subscript(index: Index) -> Iterator.Element
//	{
//		return self.sections[index]
//	}
//
//	subscript(bounds: Range<Index>) -> SubSequence
//	{
//		return self.sections[bounds]
//	}
//}




