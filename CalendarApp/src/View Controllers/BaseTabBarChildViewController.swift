//
//  BaseTabBarChildViewController.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2018-10-07.
//  Copyright © 2018 Pat Sluth. All rights reserved.
//

import UIKit

import Firebase
import FirebaseAuth
import FirebaseFirestore
import RxSwift
import RxCocoa
import RxSwiftExt

//import Alertift





class BaseTabBarChildViewController: UIViewController
{
	private var disposeBag = DisposeBag()
	
	
	
	
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		#if !APP_EXTENSION
		
		guard let navigationBar = self.navigationController?.navigationBar else { return }
		
		let userButton = UserButton(type: UIButton.ButtonType.custom)
		userButton.translatesAutoresizingMaskIntoConstraints = false
		
		if let largeTitleView = navigationBar.largeTitleView,
			let contentView = navigationBar.contentView {
			
			contentView.addSubview(userButton)
			
			NSLayoutConstraint.activate([
				userButton.heightAnchor.constraint(equalTo: contentView.heightAnchor),
				userButton.widthAnchor.constraint(equalTo: userButton.heightAnchor),
				userButton.rightAnchor.constraint(equalTo: navigationBar.layoutMarginsGuide.rightAnchor),
				userButton.topAnchor.constraint(equalTo: contentView.topAnchor)
				])
		} else {
			self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: userButton)
			
			NSLayoutConstraint.activate([
				userButton.widthAnchor.constraint(equalTo: userButton.heightAnchor),
				])
		}
		
		
		
		userButton.rx.tap
			.asDriver()
			.throttle(1.0)
			.drive(onNext: { [unowned self] in
				let navigationController = R.storyboard.main.userViewController_Navigation()!
				self.show(navigationController, sender: userButton)
			})
			.disposed(by: self.disposeBag)
		
		
		
		Auth.auth().rx.currentUser()
			.do(onNext: { user in
				if let user = user {
					userButton.setTitle(":)", for: UIControl.State.normal)
				} else {
					userButton.setTitle(nil, for: UIControl.State.normal)
				}
			})
			.subscribe()
			.disposed(by: self.disposeBag)
		
		#endif
	}
}





//class BaseTabBarChildViewController: UIViewController
//{
//	override func viewDidLoad()
//	{
//		super.viewDidLoad()
//		
//		
//		
//		
//	}
//	
//	override func viewDidAppear(_ animated: Bool)
//	{
//		super.viewDidAppear(animated)
//	}
//}




