//
//  EKCalendar.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2018-11-06.
//  Copyright © 2018 Pat Sluth. All rights reserved.
//

import Foundation
import EventKit

import PromiseKit





public extension EKEventStore
{
	public func defaultCalendar(for entityType: EKEntityType) -> EKCalendar?
	{
		switch entityType {
		case .event:		return self.defaultCalendarForNewEvents
		case .reminder:		return self.defaultCalendarForNewReminders()
		}
	}
	
	public func defaultSource(for entityType: EKEntityType) -> EKSource?
	{
		return self.defaultCalendar(for: entityType)?.source
	}
	
	public func calendar(named calendarName: String,
						 for entityType: EKEntityType) -> Promise<EKCalendar>
	{
		return Promise { resolver in
			//			let calenders = self.calendars(for: entityType).filter({ $0.title == calendarName })
			let calender = self.calendars(for: entityType).first(where: {
				$0.title == calendarName
			})
			
			if let calender = calender {
				resolver.fulfill(calender)
			} else {
				let newCalender = EKCalendar(for: entityType, eventStore: self)
				newCalender.title = calendarName
				newCalender.source = self.defaultSource(for: entityType)
				
				do {
					try self.saveCalendar(newCalender, commit: true)
					resolver.fulfill(newCalender)
				} catch {
					resolver.reject(error)
				}
			}
		}
	}
	
	public func events(with eventIdentifier: String) -> [EKEvent]
	{
		var events = [EKEvent]()
		
		guard let event = self.event(withIdentifier: eventIdentifier) else { return events }
		
		let predicate = self.predicateForEvents(withStart: event.startDate,
												end: Date.distantFuture,
												calendars: [event.calendar])
		self.enumerateEvents(matching: predicate) { event, stop in
			if event.eventIdentifier == eventIdentifier {
				events.append(event)
			}
		}
		
		return events
	}
	
	public func events(from calendar: EKCalendar) -> [EKEvent]
	{
		let pastEvents = self.events(from: calendar, for: DateInterval(start: Date.distantPast, end: Date()))
		let futureEvents = self.events(from: calendar, for: DateInterval(start: Date(), end: Date.distantFuture))
		
		return Array(Set(pastEvents + futureEvents))
	}
	
	public func events(from calendar: EKCalendar, for dateInterval: DateInterval) -> [EKEvent]
	{
		return self.events(from: [calendar], for: dateInterval)
	}
	
	public func events(from calendars: [EKCalendar], for dateInterval: DateInterval) -> [EKEvent]
	{
		let predicate = self.predicateForEvents(withStart: dateInterval.start,
												end: dateInterval.end,
												calendars: calendars)
		return self.events(matching: predicate)
	}
	
	public func eventsPromise(from calendar: EKCalendar, for dateInterval: DateInterval) -> Promise<[EKEvent]>
	{
		return self.eventsPromise(from: [calendar], for: dateInterval)
	}
	
	public func eventsPromise(from calendars: [EKCalendar], for dateInterval: DateInterval) -> Promise<[EKEvent]>
	{
		return Promise { resolver in
			
			let predicate = self.predicateForEvents(withStart: dateInterval.start,
													end: dateInterval.end,
													calendars: calendars)
			let events = self.events(matching: predicate)
			resolver.fulfill(events)
		}
	}
}




