//
//  Permission.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2018-11-06.
//  Copyright © 2018 Pat Sluth. All rights reserved.
//

import Foundation

import Permission
import PromiseKit





public extension Permission
{
	public enum RequestError: Error
	{
		case StatusMismatch(target: PermissionStatus, actual: PermissionStatus)
	}
	
	
	
	
	
	public func onRequest() -> Guarantee<PermissionStatus>
	{
		return Guarantee<PermissionStatus> { promise in
			self.request({ status in
				promise(status)
			})
		}
	}
	
	public func onRequest(status target: PermissionStatus) -> Promise<Void>
	{
		return Promise<Void> { promise in
			self.onRequest()
				.done({ status in
					if status == target {
						promise.fulfill(())
					} else {
						let error = RequestError.StatusMismatch(target: target, actual: status)
						promise.reject(error)
					}
				})
		}
	}
}




