//
//  EKEvents.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2018-10-17.
//  Copyright © 2018 Pat Sluth. All rights reserved.
//

import Foundation
import EventKit



//public struct CodableWrapper<T>: Codable
//{
//	let value: T
//
//	required public init(from decoder: Decoder) throws
//	{
//		let container = try decoder.singleValueContainer()
//
//		container.decode(<#T##type: Decodable.Protocol##Decodable.Protocol#>)
//	}
//
//	func encode(to encoder: Encoder) throws
//	{
//		var container = encoder.container(keyedBy: CodingKeys.self)
//
//		try container.encode(self.events.map({ $0.eventIdentifier }), forKey: .eventIdentifiers)
//	}
//}
//
//
//extension EKEvent: Codable
//{
//
//}


struct EKEvents: Codable, Hashable
{
	private enum CodingKeys: String, CodingKey
	{
		case eventIdentifiers
	}
	
	
	
	
	
	var events: [EKEvent]
	
	
	
	
	
	init(_ events: [EKEvent] = [])
	{
		self.events = events
	}
	
	init(from decoder: Decoder) throws
	{
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		self.events = try container.decode(Set<String>.self, forKey: .eventIdentifiers)
			.compactMap({ eventIdentifier in
				EKEventStore.default.events(with: eventIdentifier)
					.first(where: { $0.startDate >= Date() })
			})
	}
	
	func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: CodingKeys.self)
		
		try container.encode(self.events.map({ $0.eventIdentifier }), forKey: .eventIdentifiers)
	}
}




struct EventIdentifier: Codable, Hashable
{
	enum CodingKeys: String, CodingKey
	{
		case eventIdentifier
		case title
		case startDate
	}
	
	
	
	
	
	let event: EKEvent
	
	
	
	
	
	init(_ event: EKEvent)
	{
		self.event = event
	}
	
	init(from decoder: Decoder) throws
	{
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		let eventIdentifier = try container.decode(String.self, forKey: .eventIdentifier)
		let title = try container.decode(String.self, forKey: .title)
		let startDate = try container.decode(Date.self, forKey: .startDate)
		
//		let dateInterval = DateInterval(start: Date().addingTimeInterval(TimeInterval.year * -1), end: Date().addingTimeInterval(TimeInterval.year))
//		let events = EKEventStore.default
//			.events(from: EKEventStore.default.caledar, for: dateInterval)
//			.grouped(by: \EKEvent.startDate!.year)
//			//							.grouped(by: \EKEvent.eventIdentifier)
//			//							.compactMap({ $0.value.first })
//			//							.grouped(by: \EKEvent.startDate!.year)
//			.sorted(by: <, { value in
//				value.sorted(by: \EKEvent.startDate, <)
//			})
		
		
//		if let event = EKEventStore.default.event(withIdentifier: eventIdentifier) {
//			print(event.startDate == startDate)
//			self.init(event)
//		} else {
//			throw Errors.Init(EventIdentifier.self)
//		}
		if let event = EKEventStore.default.events(with: eventIdentifier)
			.first(where: { $0.title == title && $0.startDate == startDate }) {
			self.init(event)
		} else {
			throw Errors.Init
		}
		
//		}self
////			.filter({
////				$0.startDate > Date()
////			})
////			.sorted(by: \EKEvent.startDate, <)
////			.min(by: {
////				$0.startDate > $1.startDate
////			})
////			.sorted(by: \EKEvent.startDate, >)
////			.first else {
//
////			.first(where: { $0.startDate > Date() })
////			else {
//				throw Errors.Init(EventIdentifier.self)
//		}
//
//		self.init(event)
	}
	
	func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: CodingKeys.self)
		
		try container.encode(self.event.eventIdentifier, forKey: .eventIdentifier)
		try container.encode(self.event.title, forKey: .title)
		try container.encode(self.event.startDate, forKey: .startDate)
		
//		let startDate = EKEventStore.default.events(with: self.event.eventIdentifier)
//			.sorted(by: \EKEvent.startDate, <)
//			.reduce(into: [String: Date](), {
//				let fieldName = "\($0.count)"
//				$0[fieldName] = $1.startDate
//			})
//
//
	}
}





//struct Event: Codable, Hashable
//{
//	let eventIdentifier: String
//
//	var event: EKEvent? {
//		return EKEventStore.default.events(with: self.eventIdentifier)
//			.first(where: { $0.startDate >= Date() })
//	}
//
//
//
//
//
//	init(_ event: EKEvent)
//	{
//		self.eventIdentifier = event.eventIdentifier
//	}
//}




