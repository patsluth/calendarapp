//
//  Profile.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2018-10-17.
//  Copyright © 2018 Pat Sluth. All rights reserved.
//

import Foundation





struct Profile: Hashable
{
	var firstName: String
	var lastName: String
}





extension Profile: Codable
{
	private enum CodingKeys: String, CodingKey
	{
		case firstName
		case lastName
	}
}




