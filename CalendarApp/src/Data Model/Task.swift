//
//  Task.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2018-10-17.
//  Copyright © 2018 Pat Sluth. All rights reserved.
//

import Foundation





struct Task
{
	// TODO: USE THIS INSTTEAD
	// EKRecurrenceFrequency
	enum Period: String, Codable, CaseIterable
	{
		case Daily, Weekly, Monthly, Yearly
		
		var calendarComponent: Calendar.Component
		{
			switch self {
			case .Daily: return .day
			case .Weekly: return .weekOfMonth
			case .Monthly: return .month
			case .Yearly: return .year
			}
		}
	}
	
	
	
	
	
	let title: String
	let period: Period
	var ordinal: Int
	let color: UIColor = UIColor.random
}

//extension Task: Codable
//{
//	
//}

//struct Task
//{
//	var title: String
//	var period: TimeInterval
//	var totalSteps: Int
//	var completedSteps: Int
//}





//extension Task: Codable
//{
//	private enum CodingKeys: String, CodingKey
//	{
//		case firstName
//		case lastName
//	}
//}




