//
//  CalendarSelectorViewController.swift
//  CalendarApp
//
//  Created by Pat Sluth on 2018-10-27.
//  Copyright © 2018 Pat Sluth. All rights reserved.
//

import UIKit
import EventKit

import RxSwift
import RxCocoa
import RxSwiftExt
import Permission






//public protocol ModelViewProtocol: ModelConsumer
//	where Self: UIView
//{
//	associatedtype Model
////	associatedtype View: UIView
//	
//	static var nib: UINib? { get }
//}
//
//public protocol SelectableModelViewProtocol: ModelViewProtocol
//{
//	var isSelected: Bool { get set }
//}

//public typealias ModelView = UIView & ModelViewProtocol
//public typealias SelectableModelView = ModelView & SelectableModelViewProtocol

//open class ModelView<_Model>: UIView, ModelViewProtocol
//{
//	public typealias Model = _Model
//
//	public static var nib: UINib? { return nil }
//
//	open func update(with model: Model)
//	{
//		fatalError("\(#function) not implemented")
//	}
//}

//
//open class UITableViewModelCell<_Model>: UITableViewCell, ModelViewProtocol
//{
//	public typealias Model = _Model
//
//	public static var nib: UINib? { return nil }
//
//	open func update(with model: Model)
//	{
//		fatalError("\(#function) not implemented")
//	}
//}

public extension UITableView
{
//	public typealias ModelCell = UITableViewCell & ModelViewProtocol
//	public typealias SelectableModelCell = UITableView.ModelCell & SelectableModelViewProtocol
}

public extension UICollectionView
{
//	public typealias ModelCell = UICollectionViewCell & ModelViewProtocol
//	public typealias SelectableModelCell = UICollectionView.ModelCell & SelectableModelViewProtocol
}

//public extension UICollectionView
//{
////	public typealias ModelCell<M, C> = ModelView<M, C>
////		where C: UICollectionViewCell
//	open class ModelCell<_Model>: UICollectionViewCell, ModelViewProtocol
//	{
//		public typealias Model = _Model
//
//		public static var nib: UINib? { return nil }
//
//		open func update(with model: Model)
//		{
//			fatalError("\(#function) not implemented")
//		}
//	}
//}

//open class UITableViewModelCell<_Model>: UITableViewCell, UITableViewModelCellProtocol
//{
//	public typealias Model = _Model
//
//	public static var nib: UINib? {
//		return nil
//	}
//
//	open func update(with model: Model)
//	{
//
//	}
//}

public protocol Selectable
{
	
}

public protocol ModelConsumer
{
	associatedtype Model
	
	func update(with model: Model)
}

//extension ModelConsumer: Selectable
//	where Model: Selectable
//{
//
//}




